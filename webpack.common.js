const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const path = require('path')
const nodeExternals = require('webpack-node-externals')

const clientConfig = {
  entry: path.resolve(__dirname, 'src/client/App.js'),
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [{
          loader: 'style-loader' // creates style nodes from JS strings
        }, {
          loader: 'css-loader' // translates CSS into CommonJS
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([
      path.join(__dirname, 'public')
    ], {
      verbose: true
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, '/src/client/views'),
        to: path.join(__dirname, '/public/views')
      },
      {
        from: path.join(__dirname, '/src/client/assets/images'),
        to: path.join(__dirname, '/public/assets/images')
      },
      {
        from: path.join(__dirname, '/src/client/assets/docs'),
        to: path.join(__dirname, '/public/assets/docs')
      },
      {
        from: path.join(__dirname, '/src/client/assets/css'),
        to: path.join(__dirname, '/public/assets/css')
      },
      {
        from: path.join(__dirname, '/src/client/assets/fonts'),
        to: path.join(__dirname, '/public/assets/fonts')
      }
    ])
  ],
  resolve: {
    alias: {
      Root: path.resolve(__dirname, './'),
      Src: path.resolve(__dirname, './src/client/'),
      Assets: path.resolve(__dirname, 'src/client/assets/'),
      Components: path.resolve(__dirname, 'src/client/components/'),
      Modals: path.resolve(__dirname, 'src/client/components/common/modals/'),
      Utils: path.resolve(__dirname, 'src/shared/utils/'),
      Actions: path.resolve(__dirname, 'src/client/actions/'),
      Board: path.resolve(__dirname, 'src/client/components/game/components/board/')
    }
  }
}

const serverConfig = {
  entry: './src/server/server.js',
  target: 'node',
  externals: [nodeExternals()],
  output: {
    path: path.resolve(__dirname, 'server'),
    filename: 'server.js',
    libraryTarget: 'commonjs2'
  },
  devtool: 'cheap-module-source-map',
  module: {
    rules: [
      {
        test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        loader: 'file-loader',
        options: {
          name: 'public/media/[name].[ext]',
          publicPath: url => url.replace(/public/, ''),
          emit: false
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'css-loader/locals'
          }
        ]
      },
      {
        test: /.js$/,
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([
      path.resolve(__dirname, 'server')
    ], {
      verbose: true
    })
  ],
  resolve: {
    alias: {
      Utils: path.resolve(__dirname, 'src/shared/utils/')
    }
  }
}

module.exports = [clientConfig, serverConfig]
