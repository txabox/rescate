FROM ubuntu:latest
MAINTAINER Txabi Lopez <lauteilatu@hotmail.com>

RUN apt-get update && apt-get install -y \
  curl


RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash

ENV NVM_DIR=/root/.nvm
ENV SHIPPABLE_NODE_VERSION=v9.3.0

# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
ADD package.json /tmp/package.json


# From here we load our application's code in, therefore the previous docker
# "layer" thats been cached will be used if possible
WORKDIR /opt/app
ADD . /opt/app

ENV PATH="./node_modules/.bin:${PATH}"
ENV NODE_ENV production


RUN . $NVM_DIR/nvm.sh && \
  nvm install $SHIPPABLE_NODE_VERSION && \
  nvm alias default $SHIPPABLE_NODE_VERSION && \
  nvm use default && \
  npm install pm2 -g && \
  cd /tmp && npm install && \
  mkdir -p /opt/app && \
  cp -a /tmp/node_modules /opt/app/ && \
  cd /opt/app && \
  npm run start:front:prod && \
  npm run start:back:prod


EXPOSE 8080
