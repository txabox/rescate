import {expect} from 'chai'
import Signup from 'Components/users/signup/Signup'

describe('Registering users tests', () => {
  const signup = new Signup()
  const data = {
    name: 'name',
    username: 'username',
    email: 'email@email.com',
    pwd: 'pwd123',
    pwd2: 'pwd123'
  }

  it('User registration test:', () => {
    return signup.postRegisterUser(data)
      .then((response) => {
        expect(response.status).to.equal(200)
        expect(response.data.success, 'success must be true. Errors: ' + JSON.stringify(response.data.errors)).to.equal(true)
        if (!response.data.success) {
          console.error('Errors: ', JSON.stringify(response.data.errors))
        }
      })// no catch, it'll figure it out since the promise is rejected
  })
})
