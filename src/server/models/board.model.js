import mongoose from 'mongoose'
exports = module.exports = () => {
  var boardSchema = new mongoose.Schema({
    settings: {
      HEAD_PROPORTION: {
        type: Number,
        default: 9.2777777 // 1169 px width / 126 px height
      },
      COLS: {
        type: Number,
        default: 10
      },
      ROWS: {
        type: Number,
        default: 8
      },
      TEXTURE_WIDTH: {
        type: Number,
        default: 1270 // in pixels
      },
      TEXTURE_HEIGHT: {
        type: Number,
        default: 1160 // in pixels
      },
      name: {
        type: String,
        required: true,
        unique: true
      },
      title: {
        type: String,
        required: true
      },
      desc: {
        type: String
      },
      imageFilename: {
        type: String,
        required: true
      },
      bgImg: {
        type: String,
        default: ''
      },
      headDimensions: {
        bottom: {
          type: Number,
          default: 0
        },
        height: {
          type: Number,
          default: 0
        },
        left: {
          type: Number,
          default: 0
        },
        right: {
          type: Number,
          default: 0
        },
        top: {
          type: Number,
          default: 0
        },
        width: {
          type: Number,
          default: 0
        }
      },
      bodyDimensions: {
        bottom: {
          type: Number,
          default: 0
        },
        height: {
          type: Number,
          default: 0
        },
        left: {
          type: Number,
          default: 0
        },
        right: {
          type: Number,
          default: 0
        },
        top: {
          type: Number,
          default: 0
        },
        width: {
          type: Number,
          default: 0
        }
      }
    },
    walls: [
      {
        id: {
          type: String,
          required: true,
          unique: true
        },
        type: {
          type: String,
          required: true
        },
        door: {
          type: String,
          default: null
        },
        damage: {
          type: Number,
          default: 0
        },
        selectable: {
          type: Boolean,
          default: false
        }
      }
    ],
    floors: [[{
      'id': {
        type: String,
        required: true,
        unique: true
      },
      'row': {
        type: Number,
        required: true
      },
      'col': {
        type: Number,
        required: true
      },
      'type': {
        type: String,
        required: true
      },
      'isAmbulance': {
        type: Boolean,
        required: true
      },
      'isTruck': {
        type: Boolean,
        required: true
      },
      'connections': {
        'up': {
          type: String,
          default: null
        },
        'right': {
          type: String,
          default: null
        },
        'down': {
          type: String,
          default: null
        },
        'left': {
          type: String,
          default: null
        }
      },
      'walls': {
        'up': {
          type: String,
          default: null
        },
        'right': {
          type: String,
          default: null
        },
        'down': {
          type: String,
          default: null
        },
        'left': {
          type: String,
          default: null
        }
      },
      'hasHotspot': {
        type: Boolean,
        required: true
      },
      'fireStatus': {
        type: String,
        default: null
      }
    }]]
  },
  {
    timestamps: Date
  })

  mongoose.model('Board', boardSchema)
}
