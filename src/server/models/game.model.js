import mongoose from 'mongoose'
import utils from 'Utils/utils'
const types = mongoose.Schema.Types

exports = module.exports = () => {
  const gameSchema = mongoose.Schema({
    user: {
      type: types.ObjectId,
      ref: 'User',
      required: true
    },
    settings: {
      isInitialSetDone: {
        type: Boolean,
        default: false
      },
      difficulty: {
        type: String,
        required: true
      },
      numOfPlayers: {
        type: Number,
        default: 1
      },
      hotspotsLeft: {
        type: Number,
        required: true
      }
    },
    firefighters: {
      array: {
        type: Array,
        required: true
      },
      activeFirefighter: {
        type: String,
        required: true
      },
      shown: {
        type: String,
        default: ''
      }
    },
    board: {
      type: Object,
      required: true
    },
    pois: {
      array: {
        type: Array,
        required: true
      }
    },
    killedSurvivors: {
      array: {
        type: Array,
        default: [2]
      }
    },
    savedSurvivors: {
      array: {
        type: Array,
        default: [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
      }
    }
  },
  {
    timestamps: Date
  })

  // instance methods:
  gameSchema.methods.setInitialGameProperties = function(availableFirefighters, gameSettings) {
    const self = this

    this.settings.difficulty = gameSettings.difficulty
    this.settings.numOfPlayers = gameSettings.numOfPlayers

    const setHotspotsLeft = () => {
      let hotspotsLeft = 0
      switch (self.settings.difficulty) {
        case 'basic':
          hotspotsLeft = 0
          break
        case 'recruit':
          hotspotsLeft = 6
          break
        case 'veteran':
          hotspotsLeft = 9
          break
        case 'heroic':
          hotspotsLeft = 12
          break
        default:
          hotspotsLeft = 0
      }
      self.settings.hotspotsLeft = hotspotsLeft
    }

    // 10. Each player takes a Specialist card and the Firefighter and corresponding card in the color of their choice. A player can play more than one Firefighter if everyone agrees. Place remaining Firefighters, Specialist and Firefighter cards in the box
    const setFirefighters = () => {
      // get rid of all the model data. Store only the data.
      availableFirefighters = JSON.parse(JSON.stringify(availableFirefighters))
      const curFirefighters = []

      for (let i = 0; i < gameSettings.numOfFirefighters; ++i) {
        const randomIndex = utils.getRandNumBetween(0, availableFirefighters.length - 1)

        curFirefighters.push(availableFirefighters[randomIndex])

        availableFirefighters.splice(randomIndex, 1)
      }

      self.firefighters = {
        ...self.firefighters,
        array: curFirefighters,
        activeFirefighter: curFirefighters[0]._name
      }
    }

    setHotspotsLeft()
    setFirefighters()

    const initBoard = (floors, walls, cb) => {
      const triggerExplosion = (boardFloorId) => {
        const floor = utils.getFloorById(boardFloorId, floors)

        // 1. Set a fire on given floor
        if (floor.fireStatus !== 'fire') {
          floor.fireStatus = 'fire'
        }

        // 2. Place a Fire and Hot Spot marker
        if (!floor.hasHotspot) {
          floor.hasHotspot = true
        }

        //
        // 3. ... and resolve an Explosion in the Target space:
        const resolveExplosion = (initialFloorId) => {
          const resolve = (direction, initialFloorId) => {
            let curFloor = utils.getFloorById(initialFloorId, floors)

            /** ************** CHECK NEXT WALL ****************/
            const wallId = curFloor.walls[direction]
            let curWall = utils.getWallById(wallId, walls)
            // if curWall exists and it is not destroyed
            if (curWall && curWall.damage < 2) {
              // if there is no door or if it is closed
              if (!curWall.door) {
                curWall.damage++
                // and the recursive function ends here
                return
              } else if (curWall.door === 'closed') {
                // break the door and destroy the wall
                curWall.door = null
                curWall.damage = 2
                // and the recursive function ends here
                return
              }
            }

            /** ************** CHECK NEXT FLOOR ****************/
            // if there is wall but the door is open or if there is no wall,
            // check the next floor:

            let nextFloor = utils.getFloorById(curFloor.connections[direction], floors)
            // if there are no more floors left...
            if (!nextFloor) {
              // and the recursive function ends here
              return
            }

            // if it already was on fire
            if (nextFloor.fireStatus === 'fire') {
              // shockwave to the nextFloor
              resolve(direction, nextFloor.id)
            } else {
              nextFloor.fireStatus = (nextFloor.type === 'in') ? 'fire' : null
            }
          }

          resolve('up', initialFloorId)
          resolve('right', initialFloorId)
          resolve('down', initialFloorId)
          resolve('left', initialFloorId)
        }

        resolveExplosion(boardFloorId)
      }

      // 3. Choose a difficulty level: (Recruit level is similar in difficulty to the Family game, Veteran is hard and Heroic is very hard)

      const placeInitialExplosions = () => {
        // 4. first explosion
        const firstExplosion = () => {
          // Roll the black, eight-sided die to determine Target space as follows:
          // 1={3,3}, 2={3,4}, 3={3,5}, 4={3,6}, 5={4,6}, 6={4,5}, 7={4,4}, 8={4,3}.
          // Place a Fire and Hot Spot marker and resolve an Explosion in the Target space.
          const result = utils.rollColsDie()
          let explosionFloorId = ''
          switch (result) {
            case 1:
              explosionFloorId = '3_3'
              break
            case 2:
              explosionFloorId = '3_4'
              break
            case 3:
              explosionFloorId = '3_5'
              break
            case 4:
              explosionFloorId = '3_6'
              break
            case 5:
              explosionFloorId = '4_6'
              break
            case 6:
              explosionFloorId = '4_5'
              break
            case 7:
              explosionFloorId = '4_4'
              break
            case 8:
              explosionFloorId = '4_3'
              break
            default:
              console.error('Imposible result')
              break
          }

          triggerExplosion(explosionFloorId, true)
        }
        firstExplosion()

        // 5. second explosion
        const secondExplosion = () => {
          //  Roll both dice to determine the Target space.
          const diceResult = utils.rollDices()

          // If the Target space is already on Fire, roll again.
          const floorId = utils.getFloorIdFromDiceResult(diceResult)
          const floor = utils.getFloorById(floorId, floors)

          if (floor.fireStatus === 'fire') {
            return secondExplosion()
          } else {
            triggerExplosion(floorId, true)
            return diceResult
          }
        }
        let diceResult = secondExplosion()

        // 6. Third Explosion:
        const thirdExplosion = (secondExplosionDiceResult) => {
          // 1. Flip over (to show the opposite face) the black die from the previous roll
          let diceResult = utils.swapColsDie(secondExplosionDiceResult)

          // 2. and re-roll the red die to determine the Target space.
          const reRollRowsDieUntilNoFire = (diceResult) => {
            diceResult = utils.reRollRowsDie(diceResult)
            const floor = utils.getFloorById(utils.getFloorIdFromDiceResult(diceResult), floors)

            // 3. If the Target space is already on Fire, roll just the red die again.
            if (floor.fireStatus === 'fire') {
              return reRollRowsDieUntilNoFire(diceResult)
            } else {
              return diceResult
            }
          }

          diceResult = reRollRowsDieUntilNoFire(diceResult)

          // 4. Place a Fire and Hot Spot marker and resolve an Explosion in the Target space.
          const explosionFloorId = utils.getFloorIdFromDiceResult(diceResult)
          triggerExplosion(explosionFloorId, true)
        }
        thirdExplosion(diceResult)

        // 7. Fourth Explosion

        const fourthExplosion = () => {
          if (self.settings.difficulty === 'heroic') {
            // If playing at the Heroic level, roll both dice to determine the Target space for the fourth Explosion.
            // If the Target space is already on Fire, roll again.
            // Place a Fire and Hot Spot marker and resolve an Explosion in the Target space
            const diceResult = utils.rollUntilValidFloor([{key: 'fireStatus', value: 'fire'}], floors)
            const explosionFloorId = utils.getFloorIdFromDiceResult(diceResult)
            triggerExplosion(explosionFloorId, true)
          }
        }
        fourthExplosion()
      }

      // 8. Place hazzard materials:
      const placeInitialHazmats = () => {
        // Roll to place the Hazmat markers. If the Target space has a Fire marker, roll again.
        // Place a Hazmat marker in the Target space.
        let hazmats = 0
        switch (self.settings.difficulty) {
          case 'basic':
            hazmats = 0
            break
          case 'recruit':
            hazmats = 3
            break
          case 'veteran':
            hazmats = 4
            break
          case 'heroic':
            hazmats = 5
            break
          default:
            console.error('Unknown difficulty: ', self.settings.difficulty)
        }
        for (var i = 0; i < hazmats; ++i) {
          const diceResult = utils.rollUntilValidFloor([
            {key: 'fireStatus', value: 'fire'},
            {key: 'hasHazmat', value: true}
          ],
          floors)
          const floorId = utils.getFloorIdFromDiceResult(diceResult)
          const floor = utils.getFloorById(floorId, floors)
          floor.hasHazmat = true
        }
      }

      // 9. Place Points of Interest (POIs)
      const placeInitialPOIs = () => {
        //  Flip the remaining 10 Victims and 5 False Alarms markers so that the side is up and shuffle.
        let poisLeft = [ false, false, false, false, false ]
        const setInitialPOIs = () => {
          //  Flip the remaining 10 Victims and 5 False Alarms markers so that the side is up and shuffle.
          let allPOIs = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

          for (let i = 0; i < 10; ++i) {
            const indexPos = utils.getRandNumBetween(0, allPOIs.length - 1)
            poisLeft.push(allPOIs[indexPos])
            allPOIs = utils.removeItemFromArray(allPOIs, indexPos)
          }

          poisLeft = utils.shuffleArray(poisLeft)

          self.pois.array = poisLeft
        }

        setInitialPOIs()

        // Roll the dice three times, and place one POI marker with the side up on the board in each of the Target spaces.
        // If the Target space is already on Fire, roll again
        const placeThreePOIs = () => {
          for (let i = 0; i < 3; ++i) {
            const floor = utils.getRandomValidFloor(
              [
                {
                  key: 'fireStatus',
                  value: 'fire'
                },
                {
                  key: 'poi',
                  value: true
                }
              ],
              floors)

            self.pois.array.pop()

            const poi = {
              isVisible: false,
              id: poisLeft.pop()
            }
            utils.updateElementInArray(floors, floor.id, 'poi', poi)
          }
        }
        placeThreePOIs()
      }

      // 11. Place initial hotspots
      const placeInitialHotspots = () => {
        let totalHotspots = 0
        if (self.settings.difficulty === 'veteran' ||
            self.settings.difficulty === 'heroic') {
          totalHotspots += 3
        }
        if (gameSettings.numOfFirefighters === 3) {
          totalHotspots += 2
        } else if (gameSettings.numOfFirefighters > 3) {
          totalHotspots += 3
        }

        for (let i = 0; i < totalHotspots; ++i) {
          const floor = utils.getRandomValidFloor(
            [{
              key: 'hasHotspot',
              value: true
            }],
            floors)
          floor.hasHotspot = true
        }
      }

      placeInitialExplosions()
      placeInitialHazmats()
      placeInitialPOIs()
      placeInitialHotspots()

      const response = {success: true}
      cb && cb(response)
    }

    // console.log('Txabox before. floors: ', JSON.stringify(this.board.floors))
    initBoard(this.board.floors, this.board.walls)
    // console.log('Txabox after. floors: ', JSON.stringify(this.board.floors))
  }

  mongoose.model('Game', gameSchema)
}
