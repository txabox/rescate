import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'
const types = mongoose.Schema.Types

const UserSchema = mongoose.Schema({
  name: {
    type: String
  },
  username: {
    type: String,
    index: true,
    unique: true,
    required: true,
    lowercase: true,
    trim: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true
  },
  games: [{
    type: types.ObjectId,
    ref: 'Game'
  }],
  accessToken: { type: String } // Used for Remember Me
},
{
  timestamps: Date
})

// instance methods:
UserSchema.methods.comparePassword = function(newPassword, cb) {
  bcrypt.compare(newPassword, this.password, function(err, isMatch) {
    if (err) {
      return cb(err)
    }
    cb(null, isMatch)
  })
}

UserSchema.pre('save', function(next) {
  const user = this
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function(err, salt) {
      if (err) return next(err)
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) return next(err)
        user.password = hash
        next()
      })
    })
  } else {
    return next()
  }
})

const User = module.exports = mongoose.model('User', UserSchema)

// static methods:
// this method admits User.getUserByUsername.then(user => {})
User.getUserByEmail = function(email) {
  const query = {email}
  return User.findOne(query)
}

// this method admits User.getUserByUsername.then(user => {})
User.getUserByUsername = (username) => {
  const query = {username: username.toLowerCase()}
  return User.findOne(query)
}

User.createUser = function(newUser, cb) {
  newUser.save(cb)
}
