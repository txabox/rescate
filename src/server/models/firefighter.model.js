import mongoose from 'mongoose'

exports = module.exports = () => {
  const firefighterSchema = mongoose.Schema({
    _name: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    desc: {
      type: String,
      required: true
    },
    APs: {
      type: Number,
      required: true
    },
    curAPs: {
      type: Number,
      required: true
    },
    extraAPs: {
      extinguishing: {
        type: Number
      },
      curExtinguishing: {
        type: Number
      },
      freeCommand: {
        type: Number
      },
      curFreeCommand: {
        type: Number
      },
      moving: {
        type: Number
      },
      curMoving: {
        type: Number
      }
    },
    penalties: {
      extinguishing: {
        type: Number
      }
    }
  },
  {
    timestamps: Date
  })

  // instance methods:

  mongoose.model('Firefighter', firefighterSchema)
}
