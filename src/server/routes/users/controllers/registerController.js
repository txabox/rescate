import User from '../../../models/user.model'
export default (req, res, user) => {
  const name = user.name
  const username = user.username
  const email = user.email
  const password = user.pwd

  const newUser = new User({
    name,
    username,
    email,
    password
  })

  User.createUser(newUser, (error/*, product, number */) => {
    let response = {
      success: true,
      msg: 'User created successfully'
    }

    if (error) {
      response = {
        success: false,
        msg: error
      }
    }

    res.send(response)
  })
}
