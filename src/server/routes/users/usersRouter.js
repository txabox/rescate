import config from 'config'
import express from 'express'
import indexController from './controllers/indexController'
import registerController from './controllers/registerController'

import passport from 'passport'
import jwt from 'jsonwebtoken'
import User from '../../models/user.model'
require('../../config/passport')(passport)
// import loginController from './controllers/loginController';

const { check, validationResult } = require('express-validator/check')
const { matchedData } = require('express-validator/filter')

const router = express.Router()

router
  .get('/', (req, res) => {
    res.send(indexController)
  })

  .post(
    '/register',
    [
      check('name')
        .not().isEmpty().withMessage('name should not be empty'),
      check('username')
        .not().isEmpty().withMessage('username should not be empty')
        .custom(value => {
          return User.getUserByUsername(value).then(user => {
            if (user) {
              throw new Error('This username is already in use')
            } else {
              return true
            }
          })
        }),
      check('email')
        .not().isEmpty().withMessage('email should not be empty')
        // Every validator method in the validator lib is available as a
        // method in the check() APIs.
        // You can customize per validator messages with .withMessage()
        .isEmail().withMessage('must be an email')

        // Every sanitizer method in the validator lib is available as well!
        .trim()
        .normalizeEmail()

        .custom(value => {
          return User.getUserByEmail(value).then(user => {
            if (user) {
              throw new Error('This email is already in use')
            } else {
              return true
            }
          })
        }),

      // General error messages can be given as a 2nd argument in the check APIs
      check('pwd')
        .not().isEmpty().withMessage('Password should not be empty')
        .isLength({ min: 3 }).withMessage('Password must be at least 3 chars long')
        .matches(/\d/).withMessage('Password must contain one number'),

      check('pwd2', 'Confirmation password field must have the same value as the pwd field')
        .not().isEmpty().withMessage('Confirmation password should not be empty')
        .isLength({ min: 3 }).withMessage('Confirmation password must be at least 3 chars long')
        .matches(/\d/).withMessage('Confirmation password must contain one number')
        .custom((value, { req }) => value === req.body.pwd).withMessage('Password missmatch')

      // Sanitize the number of each address, making it arrive as an integer
      // sanitize('addresses.*.number').toInt()
    ],
    (req, res) => {
      try {
        validationResult(req).throw()
        // Oh look at ma' success! All validations passed!
      } catch (err) {
        // console.error(err.mapped()); // Oh noes!
        return res.status(200).json({
          success: false,
          errors: err.mapped()})
      }

      // matchedData returns only the subset of data validated by the middleware
      const user = matchedData(req)

      registerController(req, res, user)
    })

  .post(
    '/login',
    /**/
    (req, res) => {
      User.findOne({
        email: req.body.email
      }, function(err, user) {
        if (err) throw err

        if (!user) {
          res.status(401).send({
            success: false,
            msg: 'Authentication failed. User not found.'})
        } else {
          // check if password matches
          user.comparePassword(req.body.pwd, function(err, isMatch) {
            if (isMatch && !err) {
              // if user is found and password is right create a token
              var token = jwt.sign({id: user.id}, config.server.jwt.secret)
              // return the information including token as JSON
              res.json({
                success: true,
                username: user.username,
                token: token})
            } else {
              res.status(401).send({
                success: false,
                msg: 'Authentication failed. Wrong password.'})
            }
          })
        }
      })
    })

  .post('/authenticate', passport.authenticate('jwt', { session: true }),
    function(req, res) {
      if (req.user.username) {
        res.json({success: true, username: req.user.username})
      } else {
        res.json({success: false})
      }
    })

  .post('/logout', function(req, res) {
    req.logout()
    res.json({success: true})
  })

export default router
