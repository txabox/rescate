import mongoose from 'mongoose'

require('../../../models/game.model')()
require('../../../models/user.model')()
require('../../../models/firefighter.model')()

class GamesController {
  constructor() {
    this.Game = mongoose.model('Game')
    this.Board = mongoose.model('Board')
    this.User = mongoose.model('User')
    this.Firefighter = mongoose.model('Firefighter')
  }

  // CRUD:
  createGame = (req, res) => {
    // check if user is logged
    if (!req.user) {
      res.status(403).jsonp('Unauthorized')
      return
    }

    const self = this
    const gameSettings = req.body
    const user = req.user

    try {
      return this.Firefighter.find({})
        .then(availableFirefighters => {
          return this.Board.findOne({
            _id: gameSettings.selectedBoard
          })
            .then(board => {
              if (!board) {
                res.status(500).jsonp('No board is found with that name')
              }
              const newGame = new this.Game()
              newGame.user = user
              newGame.board = JSON.parse(JSON.stringify(board))
              newGame.setInitialGameProperties(availableFirefighters, gameSettings)

              return newGame.save((error, game_, number) => {
                if (error) {
                  console.error('Error creating new Game. Error: ', error)
                  res.status(400).jsonp(error)
                  return
                }

                return self.User.update(
                  { '_id': user.id },
                  { $push: { games: game_ } },
                  (error, numberAffected, rawResponse) => {
                    if (error) {
                      console.error('Error updating User. Error: ', error)
                      res.status(400).jsonp(error)
                      return
                    }
                    res.status(200).jsonp(game_)
                  })
              })
            })
            .catch(err => res.status(500).jsonp(err))
        })
        .catch(err => res.status(500).jsonp(err))
    } catch (err) {
      res.status(500).jsonp(err)
    }
  }

  getGame = (req, res) => {
    // check if user is logged
    if (!req.user) {
      res.status(403).jsonp('Unauthorized')
      return
    }

    this.Game.findOne({
      _id: req.params.id
    }, (err, game) => {
      if (err) {
        res.status(500).jsonp(err)
      }

      res.status(200).jsonp(game)
    })
  }

  updateGame = (req, res) => {
    // check if user is logged
    if (!req.user) {
      res.status(403).jsonp('Unauthorized')
      return
    }
    res.status(200).jsonp(null)
  }

  deleteGame = (req, res) => {
    // check if user is logged
    if (!req.user) {
      res.status(403).jsonp('Unauthorized')
      return
    }
    res.status(200).jsonp(null)
  }
}

export default GamesController
