import mongoose from 'mongoose'
require('../../../models/board.model')()

class BoardsController {
  constructor() {
    this.Board = mongoose.model('Board')
  }

  getAllBoards = (req, res) => {
    // check if user is logged
    if (!req.user) {
      res.status(403).jsonp('Unauthorized')
      return
    }

    this.Board.find((err, boards) => {
      if (err) {
        res.send(500, err.message)
      }

      res.status(200).jsonp(boards)
    })
  }

  getBoard = (req, res) => {
    // check if user is logged
    if (!req.user) {
      res.status(403).jsonp('Unauthorized')
      return
    }

    this.Board.findOne({
      _id: req.params.id
    }, (err, obj) => {
      if (err) {
        res.send(500, err.message)
      }

      res.status(200).jsonp(obj)
    })
  }
}

export default BoardsController
