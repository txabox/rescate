import express from 'express'
import indexController from './controllers/indexController'
import BoardsController from './controllers/BoardsController'
import GamesController from './controllers/GamesController'

const boardsController = new BoardsController()
const gamesController = new GamesController()

const router = express.Router()

router
  // INDEX
  .get('/', (req, res) => {
    res.send(indexController)
  })
  // BOARDS
  .get('/boards', (req, res) => {
    boardsController.getAllBoards(req, res)
  })
  .get('/boards/:id', (req, res) => {
    boardsController.getBoard(req, res)
  })
  .get('/boards/name/:name', (req, res) => {
    boardsController.getBoardByName(req, res)
  })
  // GAMES:
  .get('/games/:id', (req, res) => {
    gamesController.getGame(req, res)
  })
  .post('/games/', (req, res) => {
    gamesController.createGame(req, res)
  })

export default router
