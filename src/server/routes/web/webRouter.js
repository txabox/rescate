import express from 'express'

const router = express.Router()

router.get('/', (req, res) => {
  res.render('web/index.twig', {
    content: 'Hello Express and <em>TWIG</em>!'
  })
})

export default router
