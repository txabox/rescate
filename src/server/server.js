import config from 'config'
import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
// import expressValidator from 'express-validator';
import flash from 'connect-flash'
import session from 'express-session'
import passport from 'passport'
import webRouter from './routes/web/webRouter'
import apiRouter from './routes/api/apiRouter'
import usersRouter from './routes/users/usersRouter'

import mongoose from 'mongoose'
import bluebird from 'bluebird'

import User from './models/user.model'

var LocalStrategy = require('passport-local').Strategy

const app = express()

/** *********************** DATABASE CONNECTION: *************************/
mongoose.Promise = bluebird
mongoose.connect(config.server.mongodb.database, {
  useMongoClient: true
  /* other options */
})

var db = mongoose.connection
db.on('error', console.error.bind(console, 'Mongoose connection error:'))
db.once('open', function() {
  console.info('We\'re connected to mongodb!')

  /** *********************** EXPRESS SETTINGS: *************************/

  app.set('views', 'public/views/')

  // use twig as the template engine
  // Set render engine
  app.set('view engine', 'twig')
  // This section is optional and used to configure twig.
  app.set('twig options', {
    strict_variables: false
  })

  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(morgan('dev'))
  app.use(cookieParser('ThisIsNotAValidSecretChangeIt'))

  // make all assets (.js, .css, images, etc) in public accesible
  app.use(express.static('public'))

  app.use(session({
    secret: 'ThisIsNotAValidSecretChangeIt',
    saveUninitialized: true,
    resave: true
  }))

  app.use(passport.initialize())
  app.use(passport.session())

  passport.serializeUser(function(user, done) {
    done(null, user.id)
  })

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user)
    })
  })

  /**/
  passport.use(new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'pwd'
    },
    function(email, password, done) {
      User.findOne({ email }, function(err, user) {
        if (err) {
          return done(err)
        }
        if (!user) {
          return done(null, false, { message: 'Incorrect email.' })
        }

        user.comparePassword(password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            // var token = jwt.sign(user, config.secret);

            return done(null, user)
            // return the information including token as JSON
            // res.json({success: true, token: 'JWT ' + token});
          } else {
            return done(null, false, { message: 'Incorrect password.' })
            // res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
          }
        })
      })
    }
  ))
  /**/
  // expressValidator is now created straight in the routes

  // Connect flash
  app.use(flash())

  // Global Vars
  app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    res.locals.error = req.flash('error')
    next()
  })

  // To make this server CORS-ENABLE
  app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
  })

  /** *********************** EXPRESS ROUTERS: *************************/
  // API Router
  app.use('/api', apiRouter)
  // Login Register Router
  app.use('/users', usersRouter)

  // The rest of routes go to react webApp
  app.use('/*', webRouter)

  /** *********************** EXPRESS INIT: *************************/
  app.listen(config.server.port, () => {
    console.info('Express listening on port ', config.server.port)
  })
})
