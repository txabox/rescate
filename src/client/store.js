import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux'
import thunk from 'redux-thunk'
// Logger with default options
// import logger from 'redux-logger'
import globals from './reducers/common/globalsReducer'
import index from './reducers/index/indexReducer'
import settings from './reducers/settings/settingsReducer'
import game from './reducers/game/gameReducer'
import users from './reducers/users/usersReducer'

const reducers = combineReducers({
  globals,
  index,
  settings,
  game,
  users
})

// middlewares:
// const mutatingErrorChecker = store => next => action => {
//   let prevState = store.getState()
//   let result = next(action)
//   let curState = store.getState()
//
//   if (JSON.stringify(prevState) === JSON.stringify(curState)) {
//     console.warn('prevState and curState is exactly the same in action ' + action.type + '. action: ', action, ', state: ', curState)
//     // debugger;
//   }
//
//   return result
// }

export default createStore(
  reducers,
  applyMiddleware(
    thunk
    // , logger
    // , mutatingErrorChecker
  ))
