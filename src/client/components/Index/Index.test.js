import React from 'react'
import { configure, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
// import configureStore from 'redux-mock-store'
import Adapter from 'enzyme-adapter-react-16'
import { Index } from './Index'

configure({ adapter: new Adapter() })

describe('Index Component', () => {
  let wrapper
  let tree

  beforeEach(() => {
    wrapper = shallow(<Index users={{}} />)
    tree = toJson(wrapper)
  })

  it('+++ render the DUMB component', () => {
    expect(wrapper.length).toEqual(1)
  })

  it('+++ compare snapshots', () => {
    expect(tree).toMatchSnapshot()
  })
})
