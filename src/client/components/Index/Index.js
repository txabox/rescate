import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Image, Glyphicon, Button } from 'react-bootstrap'
import styled from 'styled-components'

import { connect } from 'react-redux'

import Header from 'Components/common/header/Header'
import portadaImg from './img/index.jpg'

const StyledIndex = styled.div`
  height: 100%;
  .mb30px {
    margin-bottom: 30px;
  }

  img {
    margin-top: 20px;
  }

  .create-game-btn-container {
    margin-top: 20px;
    text-align: center;
  }

  .download-rules-btn {
    text-align: center;
    margin-top: 20px;
  }
`

export class Index extends React.Component {
  downloadRules() {
    const response = {
      file: '/public/assets/docs/rules.pdf'
    }
    // server sent the url to the file!
    // now, let's download:
    window.location.href = response.file
    // you could also do:
    // window.open(response.file);
  }

  render() {
    return (
      <StyledIndex>
        <Header />

        <div className="container mb30px">
          <Image src={portadaImg} className="img-responsive center-block" />

          {
            this.props.users.isLogged
              ? <div className="create-game-btn-container">
                <Link to="settings" className="btn btn-success btn-lg" role="button">
                  Create New Game
                </Link>
              </div>
              : <div className="create-game-btn-container">
                <p><strong>You need to be logged to play this game.</strong></p>
                <Link to="/login" className="btn btn-success btn-lg" role="button">
                  Login
                </Link>
              </div>
          }

          <div className="download-rules-btn">
            <Button bsStyle="info" href="assets/docs/rules.pdf" target="_blank">
              <Glyphicon glyph="download-alt" />&nbsp;&nbsp;Download Rules
            </Button>
          </div>

        </div>

      </StyledIndex>
    )
  }
}

Index.propTypes = {
  users: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    users: state.users
  }
}

export default connect(mapStateToProps)(Index)
