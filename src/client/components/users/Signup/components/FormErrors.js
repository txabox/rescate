import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'react-bootstrap'

class FormErrors extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      formErrors: props.formErrors
    }
  }

  isValid = () => {
    let isValid = true

    Object.keys(this.state.formErrors).map((fieldName) => {
      if (this.state.formErrors[fieldName].length > 0) {
        isValid = false
        return isValid
      }
    })
    return isValid
  }

  render() {
    return (
      <div className={this.isValid() ? 'hidden' : '' }>
        {Object.keys(this.state.formErrors).map((fieldName, i) => {
          if (this.state.formErrors[fieldName].length > 0) {
            return (
              <Alert bsStyle="danger" key={i}>{fieldName}: {this.state.formErrors[fieldName]}</Alert>
            )
          } else {
            return ''
          }
        })}
      </div>
    )
  }
}

FormErrors.propTypes = {
  formErrors: PropTypes.object.isRequired
}

export default FormErrors
