import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import styled from 'styled-components'

import validator from 'validator'
import { Link } from 'react-router-dom'
import { Alert, Glyphicon } from 'react-bootstrap'

import Header from 'Components/common/header/Header'
import FormErrors from './components/FormErrors'

const StyledSignup = styled.div`
  .account-wall {
    margin-top: 20px;
    padding: 40px 20px 20px;
    background-color: #f7f7f7;
    box-shadow: 0 2px 2px rgba(0, 0, 0, .3);
  }

  .login-title {
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
  }
`

class Signup extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      username: '',
      email: '',
      pwd: '',
      pwd2: '',
      formErrors: {
        name: '',
        username: '',
        email: '',
        pwd: '',
        pwd2: ''
      },
      // we’ll use these to enable or disable the form submit button
      nameValid: false,
      usernameValid: false,
      emailValid: false,
      pwdValid: false,
      pwd2Valid: false,
      formValid: false,
      showRegistrationSuccessMsg: false
    }
  }

  componentWillMount() {
    if (this.props.isLogged) {
      const { history } = this.props
      history.replace('/')
    }
  }

  add = (a, b) => a + b;

  setErrorClass(fieldName) {
    return (this.state.formErrors[fieldName].length === 0 ? '' : 'has-error')
  }

  // When a msg is provided the error comes straight from the server validation
  validateField = (fieldName, value, msg) => {
    let fieldValidationErrors = this.state.formErrors
    let nameValid = this.state.nameValid
    let usernameValid = this.state.usernameValid
    let emailValid = this.state.emailValid
    let pwdValid = this.state.pwdValid
    let pwd2Valid = this.state.pwd2Valid

    switch (fieldName) {
      case 'name':
        if (msg) {
          nameValid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          nameValid = !validator.isEmpty(value)
          fieldValidationErrors[fieldName] = nameValid ? '' : ' can not be empty'
        }
        break
      case 'username':
        if (msg) {
          usernameValid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          usernameValid = !validator.isEmpty(value)
          fieldValidationErrors[fieldName] = usernameValid ? '' : ' can not be empty'
        }
        break
      case 'email':

        if (msg) {
          emailValid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          emailValid = validator.isEmail(value)
          fieldValidationErrors[fieldName] = emailValid ? '' : ' is invalid'
        }
        break
      case 'pwd':
        if (msg) {
          pwdValid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          pwdValid = value.length >= 3
          fieldValidationErrors.pwd = pwdValid ? '' : ' is too short'
          if (pwdValid) {
            pwd2Valid = validator.equals(this.state.pwd, this.state.pwd2)
            fieldValidationErrors.pwd2 = pwd2Valid ? '' : 'Confirmation password doesn\'t match'
          }
        }
        break
      case 'pwd2':
        if (msg) {
          pwd2Valid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          pwd2Valid = value.length >= 3
          fieldValidationErrors.pwd2 = pwd2Valid ? '' : ' is too short'
          if (pwdValid) {
            pwd2Valid = validator.equals(this.state.pwd, this.state.pwd2)
            fieldValidationErrors.pwd2 = pwd2Valid ? '' : 'Confirmation password doesn\'t match'
          }
        }
        break
      default:
        break
    }

    this.setState({
      formErrors: fieldValidationErrors,
      nameValid,
      usernameValid,
      emailValid,
      pwdValid,
      pwd2Valid
    }, this.validateForm)
  }

  validateForm() {
    this.setState({
      formValid: this.state.nameValid && this.state.usernameValid && this.state.emailValid && this.state.pwdValid && this.state.pwd2Valid
    })
  }

  onChange = (e) => {
    // Because we named the inputs to match their corresponding values in state, it's
    // super easy to update the state
    const state = this.state
    const {name, value} = e.target
    state[name] = value
    this.setState(
      state,
      () => { this.validateField(name, value) }
    )
  }

  postRegisterUser = (data) => {
    const { name, username, email, pwd, pwd2 } = data

    const url = '/users/register'
    return axios.post(url, { name, username, email, pwd, pwd2 })
    // .then(
    //   (onFullfilledResult) => {
    //   },
    //
    //   (onRejectedResult) => {
    //   }
    // )
    // .catch( (onRejectedResult) => {
    // });
  }

  onSubmit = (e) => {
    e.preventDefault()

    this.postRegisterUser(this.state)
      .then((result) => {
        const data = result.data
        if (!data.errors) {
          if (data.success) {
            this.setState({
              showRegistrationSuccessMsg: true
            })
          } else {
            alert('Error saving the User in the Database: ' + data.msg)
          }
        } else {
          const errors = data.errors
          for (let key in errors) {
            if (errors.hasOwnProperty(key)) {
              const error = errors[key]
              this.validateField(error.param, error.value, error.msg)
            }
          }
        }

        // access the results here....
      })
      .catch(error => {
        console.error(error.response)
        alert('Error: (' + error.response.status + ') ' + error.response.statusText)
      })
    // get our form data out of state
    // const { name, username, email, pwd, pwd2 } = this.state;
    //
    // axios.post('/users/register', { name, username, email, pwd, pwd2 })
    //   .then((result) => {
    //     const data = result.data;
    //     if(!data.errors){
    //
    //       if (data.success) {
    //         this.setState({
    //           showRegistrationSuccessMsg: true
    //         });
    //       }
    //       else {
    //         alert('Error saving the User in the Database: ' + data.msg);
    //       }
    //     }
    //     else {
    //       const errors = data.errors;
    //       for (let key in errors) {
    //         if (errors.hasOwnProperty(key)) {
    //           const error = errors[key];
    //           this.validateField(error.param, error.value, error.msg );
    //         }
    //       }
    //     }
    //
    //     //access the results here....
    //   })
    //   .catch(error => {
    //     console.error(error.response);
    //     alert( 'Error: (' + error.response.status + ') ' + error.response.statusText);
    //   });
  }

  render() {
    const { name, username, email, pwd, pwd2 } = this.state
    return (
      <StyledSignup>
        <Header />

        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-sm-offset-3">

              <h1 className="text-center login-title">Register</h1>
              <div className="account-wall">
                <form className="form-horizontal">

                  <fieldset className={this.state.showRegistrationSuccessMsg ? 'hidden' : '' }>

                    <div className={`form-group ${this.setErrorClass('name')}`}>
                      <label className="col-md-4 control-label" htmlFor="name">Name</label>

                      <div className="col-md-8">
                        <input id="name" name="name" value={name} type="text" placeholder="Name" className="form-control input-md" onChange={this.onChange} />

                      </div>
                    </div>

                    <div className={`form-group ${this.setErrorClass('username')}`}>
                      <label className="col-md-4 control-label" htmlFor="username">User Name</label>
                      <div className="col-md-8">
                        <input id="username" name="username" value={username} type="text" placeholder="User Name" className="form-control input-md" required="" onChange={this.onChange} />

                      </div>
                    </div>

                    <div className={`form-group ${this.setErrorClass('email')}`}>
                      <label className="col-md-4 control-label" htmlFor="email">Email</label>
                      <div className="col-md-8">
                        <input id="email" name="email" value={email} type="email" placeholder="Email" className="form-control input-md" required="" onChange={this.onChange} />

                      </div>
                    </div>

                    <div className={`form-group ${this.setErrorClass('pwd')}`}>
                      <label className="col-md-4 control-label" htmlFor="pwd">Password</label>
                      <div className="col-md-8">
                        <input id="pwd" name="pwd" value={pwd} type="password" placeholder="Password" className="form-control input-md" required="" onChange={this.onChange} />

                      </div>
                    </div>

                    <div className={`form-group ${this.setErrorClass('pwd2')}`}>
                      <label className="col-md-4 control-label" htmlFor="pwd2">Repeat Password</label>
                      <div className="col-md-8">
                        <input id="pwd2" name="pwd2" value={pwd2} type="password" placeholder="Repeat Password" className="form-control input-md" required="" onChange={this.onChange} />

                      </div>
                    </div>

                    <FormErrors formErrors={this.state.formErrors} />

                    <div className="form-group">
                      <label className="col-md-4 control-label" htmlFor="submit"></label>
                      <div className="col-md-8">
                        <button id="submit" name="submit" className="btn btn-primary" onClick={this.onSubmit} disabled={!this.state.formValid}>Submit</button>
                      </div>
                    </div>
                  </fieldset>

                  <fieldset className={this.state.showRegistrationSuccessMsg ? '' : 'hidden' }>
                    <Alert bsStyle="success">
                      You are registered and can now login. <Link to="/login"><Glyphicon glyph="log-in" /> Login</Link>
                    </Alert>
                  </fieldset>
                </form>
                <div className="container">
                  <div className="row">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </StyledSignup>
    )
  }
}

Signup.propTypes = {
  isLogged: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired
}

export default Signup
