import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import axios from 'axios'
import storeJS from 'store2'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as UsersActionCreators from 'Actions/UsersActionCreators'

import validator from 'validator'

import Header from 'Components/common/header/Header'
import FormErrors from './components/FormErrors'
import defaultProfileImg from './img/defaultProfileImg.jpg'

const StyledLogin = styled.div`
  .form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;

    .form-signin-heading {
      margin-bottom: 10px;
    }

    .checkbox {
      margin-bottom: 10px;
      font-weight: normal;
    }

    .form-control {
      position: relative;
      font-size: 16px;
      height: auto;
      padding: 10px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;

      &:focus {
        z-index: 2;
      }
    }

    input {
      &.name {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
      }

      &.pwd {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }
    }
  }

  .account-wall {
    margin-top: 20px;
    padding: 40px 0 20px;
    background-color: #f7f7f7;
    box-shadow: 0 2px 2px rgba(0, 0, 0, .3);
  }

  .login-title {
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
  }

  .profile-img {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
  }

  .need-help {
    margin-top: 10px;
  }

  .new-account {
    display: block;
    margin-top: 10px;
    margin-bottom: 30px;
  }

  .form-group {
    margin-bottom: 0;
  }
`

class Login extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      pwd: '',
      formErrors: {
        email: '',
        pwd: ''
      },
      // we’ll use these to enable or disable the form submit button
      emailValid: false,
      pwdValid: false,
      formValid: false
    }
  }

  componentWillMount() {
    if (this.props.isLogged) {
      const { history } = this.props
      history.replace('/')
    }
  }

  // When a msg is provided the error comes straight from the server validation
  validateField = (fieldName, value, msg) => {
    let fieldValidationErrors = this.state.formErrors

    let emailValid = this.state.emailValid
    let pwdValid = this.state.pwdValid

    switch (fieldName) {
      case 'email':
        if (msg) {
          emailValid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          emailValid = validator.isEmail(value)
          fieldValidationErrors[fieldName] = emailValid ? '' : ' is invalid'
        }
        break
      case 'pwd':
        if (msg) {
          pwdValid = false
          fieldValidationErrors[fieldName] = msg
        } else {
          pwdValid = value.length >= 3
          fieldValidationErrors.pwd = pwdValid ? '' : ' is too short'
        }
        break
      default:
        break
    }

    this.setState({
      formErrors: fieldValidationErrors,
      emailValid,
      pwdValid
    }, this.validateForm)
  }

  validateForm() {
    this.setState({
      formValid: this.state.emailValid && this.state.pwdValid
    })
  }

  setErrorClass(fieldName) {
    return (this.state.formErrors[fieldName].length === 0 ? '' : 'has-error')
  }

  onChange = (e) => {
    // Because we named the inputs to match their corresponding values in state, it's
    // super easy to update the state
    const state = this.state
    const {name, value} = e.target
    state[name] = value
    this.setState(
      state,
      () => { this.validateField(name, value) }
    )
  }

  onSubmit = (e) => {
    e.preventDefault()
    // get our form data out of state
    const { email, pwd } = this.state
    const params = { email, pwd }
    const config = {}

    axios.post('/users/login', params, config)
      .then((result) => {
        const data = result.data

        if (data.success) {
          storeJS.set('token', data.token)
          this.props.actions.login(data.username)
          // redirect to index page
          this.props.history.replace('/')
          window.location.reload()
        } else {
          console.error('Error login. data: ', data)
          alert('Login error: ' + data.msg)
        }

        // access the results here....
      })
      .catch(error => {
        console.error('Error login. error: ', error.response)
        alert('Error: (' + error.response.status + ') ' + error.response.statusText)
      })
  }

  render() {
    const { email, pwd } = this.state
    return (
      <StyledLogin>
        <Header />

        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
              <h1 className="text-center login-title">Sign in to continue to the Game</h1>
              <div className="account-wall">
                <img className="profile-img" src={defaultProfileImg} alt="" />
                <form className="form-signin">

                  <div className={`form-group ${this.setErrorClass('email')}`}>
                    <input id="email" name="email" value={email} className="form-control email" onChange={this.onChange} type="email" placeholder="Email" required autoFocus />
                  </div>

                  <div className={`form-group ${this.setErrorClass('pwd')}`}>
                    <input id="pwd" name="pwd" value={pwd} className="form-control pwd" onChange={this.onChange} type="password" placeholder="Password" required />
                  </div>

                  <FormErrors formErrors={this.state.formErrors} />

                  <button className="btn btn-lg btn-primary btn-block" onClick={this.onSubmit} disabled={!this.state.formValid} >
                  Sign in</button>

                  <label className="checkbox pull-left hidden">
                    <input type="checkbox" value="remember-me" />
                    Remember me
                  </label>

                  <a href="#" className="pull-right need-help hidden">Need help? </a><span className="clearfix"></span>

                </form>

              </div>

              <Link to="signup" className="text-center new-account">Create an account</Link>

            </div>
          </div>
        </div>
      </StyledLogin>
    )
  }
}

Login.propTypes = {
  isLogged: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(UsersActionCreators, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(Login)
