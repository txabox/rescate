import React from 'react'
import PropTypes from 'prop-types'
import Measure from 'react-measure'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as BoardActionCreators from 'Actions/BoardActionCreators'

import BoardHeader from './components/BoardHeader/BoardHeader'
import BoardBody from './components/BoardBody/BoardBody'

const StyledBoard = styled.table`
  width: 100%;
`

class Board extends React.Component {
  componentWillMount() {
    this.props.actions.board.setBgImg(`/assets/images/Game/Board/${this.props.imageFilename}.jpg`)
  }

  updateTheadDimensions = (headDimensions) => {
    headDimensions.height = headDimensions.width / this.props.HEAD_PROPORTION

    this.props.actions.board.setHeadDimensions(headDimensions)
  }

  updateTbodyDimensions = (bodyDimensions) => {
    const { TEXTURE_WIDTH, TEXTURE_HEIGHT, COLS, ROWS } = this.props
    this.props.actions.board.setBodyDimensions(
      bodyDimensions,
      TEXTURE_WIDTH,
      TEXTURE_HEIGHT,
      COLS,
      ROWS
    )
  }

  render() {
    // console.log('Rendering Body', this.props)
    return (
      <StyledBoard>

        <Measure
          bounds
          onResize={(contentRect) => {
            this.updateTheadDimensions(contentRect.bounds)
          }}
        >
          {({ measureRef }) =>
            <BoardHeader
              getParent={() => { return this }}
              innerRef={measureRef}
              headDimensions = {this.props.headDimensions} />
          }
        </Measure>

        <Measure
          bounds
          onResize={(contentRect) => {
            this.updateTbodyDimensions(contentRect.bounds)
          }}
        >
          {({ measureRef }) =>
            <BoardBody innerRef={measureRef} />
          }
        </Measure>

      </StyledBoard>
    )
  }
}

Board.propTypes = {
  imageFilename: PropTypes.string.isRequired,
  HEAD_PROPORTION: PropTypes.number.isRequired,
  TEXTURE_WIDTH: PropTypes.number.isRequired,
  TEXTURE_HEIGHT: PropTypes.number.isRequired,
  COLS: PropTypes.number.isRequired,
  ROWS: PropTypes.number.isRequired,
  headDimensions: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  const {imageFilename, HEAD_PROPORTION, TEXTURE_WIDTH, TEXTURE_HEIGHT, COLS, ROWS, headDimensions} = state.game.board.settings
  return {
    imageFilename,
    HEAD_PROPORTION,
    TEXTURE_WIDTH,
    TEXTURE_HEIGHT,
    COLS,
    ROWS,
    headDimensions
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      board: bindActionCreators(BoardActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Board)
