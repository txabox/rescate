import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Floors from './components/Floors/Floors'
import Walls from './components/Walls/Walls'

const StyledBoardBody = styled.tbody`
  td{
    position: relative;
  }
`

const BoardBody = ({innerRef}) => {
  // console.log('Rendering BoardBody')
  return <StyledBoardBody innerRef={innerRef}>
    <tr>
      <td colSpan="10">
        <Floors />
        <Walls />
      </td>
    </tr>
  </StyledBoardBody>
}

BoardBody.propTypes = {
  innerRef: PropTypes.func.isRequired
}

export default BoardBody
