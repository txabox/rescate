import React from 'react'
import styled from 'styled-components'

import hotspotImg from './img/hotspot.png'

const StyledComponent = styled.div`
  img {
    position: absolute;
    width: 20%;
    top: 5px;
    right: 5px;
  }
`

class HotspotToken extends React.Component {
  render() {
    return <StyledComponent>
      <img src={`/${hotspotImg}`}/>
    </StyledComponent>
  }
}

export default HotspotToken
