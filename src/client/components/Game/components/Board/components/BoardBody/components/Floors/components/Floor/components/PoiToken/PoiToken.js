import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import questionMarkImg from './img/questionMark.png'
import falseAlarmImg from './img/falseAlarm.png'
import person1Img from './img/person1.png'
import person2Img from './img/person2.png'
import person3Img from './img/person3.png'
import person4Img from './img/person4.png'
import person5Img from './img/person5.png'
import person6Img from './img/person6.png'
import person7Img from './img/person7.png'
import person8Img from './img/person8.png'
import person9Img from './img/person9.png'
import person10Img from './img/person10.png'
import person11Img from './img/person11.png'
import person12Img from './img/person12.png'

const IMAGE_MAP = new Map()
IMAGE_MAP.set(false, falseAlarmImg)
IMAGE_MAP.set(1, person1Img)
IMAGE_MAP.set(2, person2Img)
IMAGE_MAP.set(3, person3Img)
IMAGE_MAP.set(4, person4Img)
IMAGE_MAP.set(5, person5Img)
IMAGE_MAP.set(6, person6Img)
IMAGE_MAP.set(7, person7Img)
IMAGE_MAP.set(8, person8Img)
IMAGE_MAP.set(9, person9Img)
IMAGE_MAP.set(10, person10Img)
IMAGE_MAP.set(11, person11Img)
IMAGE_MAP.set(12, person12Img)

const StyledComponent = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  width: 100%;
  justify-content: center;
  position: absolute;
  resize: both;

  img {
    resize: both;
    width: 58%;
  }
`
const PoiToken = ({poi}) => {
  return <StyledComponent>
    { poi.isVisible
        ? <img src={`/${IMAGE_MAP.get(poi.id)}`} />
        : <img src={`/${questionMarkImg}`}/>
    }
  </StyledComponent>
}

PoiToken.propTypes = {
  poi: PropTypes.object
}

export default PoiToken
