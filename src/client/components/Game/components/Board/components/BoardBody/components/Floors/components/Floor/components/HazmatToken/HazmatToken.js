import React from 'react'
import styled from 'styled-components'

import hazmatImg from './img/hazmat.png'

const StyledComponent = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  width: 100%;
  justify-content: center;
  position: absolute;
  resize: both;

  img {
    resize: both;
    width: 65%;
  }
`

class HazmatToken extends React.Component {
  render() {
    return <StyledComponent>
      <img src={`/${hazmatImg}`}/>
    </StyledComponent>
  }
}

export default HazmatToken
