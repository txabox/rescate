import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import UniqueId from 'react-html-id'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as FloorsActionCreators from 'Actions/FloorsActionCreators'

import Floor from './components/Floor/Floor'

const StyledFloors = styled.table`
  width: 100%
`

class Floors extends React.Component {
  constructor(props) {
    super(props)
    UniqueId.enableUniqueIds(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.props.boardBgImg !== nextProps.boardBgImg ||
      JSON.stringify(this.props.floors) !== JSON.stringify(nextProps.floors)) {
      return true
    }
    return false
  }

  handleClick = (floor) => {
    if (!floor.selectable) {
      return false
    }
    console.warn('Clicked on floor ' + floor.id + ': ', floor)
  }

  render() {
    // console.log('Rendering Floors')
    return <StyledFloors>
      <tbody>
        {this.props.floors.array.map(floorRow => {
          const curRowCols = floorRow.map((floor) => {
          // TODO: This selectable is fake just to check that it works fine
            floor.selectable = floor.type === 'out'
            return <Floor
              key={`floor_${floor.id}`}
              floor={floor}
              boardBgImg={this.props.boardBgImg}
              floorsDimensions = {this.props.floors.dimensions}
              bgSize={this.props.floors.bgSize}
              handleClick={this.handleClick} />
          })
          return <tr key={this.nextUniqueId()}>
            {curRowCols}
          </tr>
        })}

      </tbody>
    </StyledFloors>
  }
}

Floors.propTypes = {
  boardBgImg: PropTypes.string.isRequired,
  floors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    boardBgImg: state.game.board.settings.bgImg,
    floors: state.game.board.floors
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      floors: bindActionCreators(FloorsActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Floors)
