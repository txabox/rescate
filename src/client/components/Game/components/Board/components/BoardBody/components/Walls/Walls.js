import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as WallsActionCreators from 'Actions/WallsActionCreators'

import utils from 'Utils/utils'
import Wall from './components/Wall/Wall'

const StyledWalls = styled.div`
  position: absolute;
  width: 100%;
  pointer-events: none;
  cursor: default;
  top: 0;
  height: ${props => props.bodyDimensions.height}px
`
class Walls extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (
      JSON.stringify(this.props.bodyDimensions) !== JSON.stringify(nextProps.bodyDimensions) ||
      JSON.stringify(this.props.walls) !== JSON.stringify(nextProps.walls)
    ) {
      return true
    }
    return false
  }

  toogleDoor = (wallId) => {
    let door = utils.getWallById(wallId, this.props.walls).door
    if (door === 'open') {
      door = 'closed'
    } else if (door === 'closed') {
      door = 'open'
    }
    this.props.actions.walls.setDoor(wallId, door)
  }

  handleClick = (wall) => {
    if (!wall.selectable) {
      return false
    }
    console.warn('Clicked on wall ' + wall.id + ': ', wall)
    this.toogleDoor(wall.id)
  }

  render() {
    // console.log('Rendering Walls')
    return <StyledWalls bodyDimensions={this.props.bodyDimensions}>
      {
        this.props.walls.array.map((wall) => {
          return <Wall
            key={wall.id}
            wall={wall}
            dimensions={this.props.walls.dimensions[wall.type]}
            handleClick={this.handleClick}
            setIsSelectable={this.props.actions.walls.setIsSelectable} />
        })
      }
    </StyledWalls>
  }
}

Walls.propTypes = {
  bodyDimensions: PropTypes.object.isRequired,
  walls: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    walls: state.game.board.walls,
    bodyDimensions: state.game.board.settings.bodyDimensions
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      walls: bindActionCreators(WallsActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Walls)
