import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import fireTokenImg from './img/fire.png'
import smokeTokenImg from './img/smoke.png'

const StyledComponent = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  width: 100%;
  justify-content: center;
  position: absolute;
  resize: both;

  img {
    resize: both;
    width: 58%;
  }
`

const FireToken = ({fireStatus}) => {
  return <StyledComponent>
    <img src={fireStatus === 'fire' ? `/${fireTokenImg}` : `/${smokeTokenImg}`}/>
  </StyledComponent>
}

FireToken.propTypes = {
  fireStatus: PropTypes.string
}

export default FireToken
