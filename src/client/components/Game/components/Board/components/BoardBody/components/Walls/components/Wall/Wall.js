import React from 'react'
import PropTypes from 'prop-types'
import styled, {css} from 'styled-components'

import BoardDoor from './components/boardDoor/BoardDoor'
import brokenWallImg from './img/brokenWall.png'

const StyledWall = styled.div`
  .wall {
    top: ${props => props.inherited.position.top}px;
    left: ${props => props.inherited.position.left}px;
    width: ${props => props.inherited.dimensions.width}px;
    height: ${props => props.inherited.dimensions.height}px;
    position: absolute;
    pointer-events: all;
    background-color: #fff;
    min-height: 5px;
    min-width: 5px;
    overflow: visible;
    display: flex;
    justify-content: center;
    align-items: center;

    ${props => props.inherited.wall.selectable && css`
      cursor: pointer;

      &:hover {
        filter: alpha(opacity=75);
        opacity: .75;
      }
    `}

    ${props => props.inherited.wall.damage === 1 && css`
      background-color: transparent;
      background-size: ${props => props.inherited.dimensions.floorW}px;
      background-image: url("/${brokenWallImg}");
    `}

    ${props => props.inherited.wall.damage > 1 && css`
      display: none;
    `}
  }

  .wall-end-0 {
    position: absolute;
    background-color: #fff;
    width: ${props => props.inherited.wallEnd0.width}px;
    height: ${props => props.inherited.wallEnd0.height}px;
    top: ${props => props.inherited.wallEnd0.top}px;
    left: ${props => props.inherited.wallEnd0.left}px;
  }

  .wall-end-1 {
    position: absolute;
    background-color: #fff;
    width: ${props => props.inherited.wallEnd1.width}px;
    height: ${props => props.inherited.wallEnd1.height}px;
    top: ${props => props.inherited.wallEnd1.top}px;
    left: ${props => props.inherited.wallEnd1.left}px;
  }
`

class Wall extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      position: {
        top: 0,
        left: 0
      },
      wallEnd0: {},
      wallEnd1: {}
    }

    // TODO: Remove this. This selectable is fake just to check that it works fine to open/close doors but when the game is completed only some walls will be selectable depending on many variables
    if (props.wall.door !== null) {
      props.setIsSelectable(props.wall.id, true)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.dimensions.width === nextProps.dimensions.width) return
    const {width, halfW, height, halfH} = nextProps.dimensions
    let {top, left} = 0

    let wallEnd0 = {}
    let wallEnd1 = {}

    if (this.props.wall.type === 'horizontal') {
      // WALL:
      top = this.getFloorPosition(this.props.wall.connections[1], nextProps).top - halfH
      left = this.getFloorPosition(this.props.wall.connections[0], nextProps).left

      // wallEnd0:
      wallEnd0.height = height
      wallEnd0.width = height
      wallEnd0.top = top
      wallEnd0.left = left - halfH

      // wallEnd1:
      wallEnd1.height = height
      wallEnd1.width = height
      wallEnd1.top = top
      wallEnd1.left = left + width - halfH
    } else if (this.props.wall.type === 'vertical') {
      top = this.getFloorPosition(this.props.wall.connections[0], nextProps).top
      left = this.getFloorPosition(this.props.wall.connections[1], nextProps).left - halfW

      // wallEnd0:
      wallEnd0.width = width
      wallEnd0.height = width // they are squares
      wallEnd0.top = top - halfW
      wallEnd0.left = left

      // wallEnd1:
      wallEnd1.width = width
      wallEnd1.height = width // they are squares
      wallEnd1.top = top + height - halfW
      wallEnd1.left = left
    } else {
      console.error('Unknown object type')
    }

    const position = {
      top,
      left
    }

    this.setState({
      wallEnd0,
      wallEnd1,
      position
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      JSON.stringify(this.props.dimensions) !== JSON.stringify(nextProps.dimensions) ||
      JSON.stringify(nextProps.wall) !== JSON.stringify(this.props.wall)) {
      return true
    }
    return false
  }

  getFloorPosition = (floorId, nextProps) => {
    const idSplitted = floorId.split('_')
    const row = idSplitted[0]
    const col = idSplitted[1]
    let position = {
      top: row * nextProps.dimensions.floorH,
      left: col * nextProps.dimensions.floorW
    }
    return position
  }

  render() {
    // console.log('Rendering Wall')
    return <StyledWall inherited={{ ...this.props, ...this.state }} >

      <div
        onClick={() => this.props.handleClick(this.props.wall)}
        className="wall"
        ref={(div) => { this.$wall = div }}>

        <BoardDoor
          door={this.props.wall.door}
          wallType={this.props.wall.type} />

      </div>

      <div className="wall-end-0" />
      <div className="wall-end-1" />

    </StyledWall>
  }
}

Wall.propTypes = {
  wall: PropTypes.object.isRequired,
  dimensions: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  setIsSelectable: PropTypes.func.isRequired
}

export default Wall
