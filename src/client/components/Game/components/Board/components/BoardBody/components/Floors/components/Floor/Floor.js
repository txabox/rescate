import React from 'react'
import PropTypes from 'prop-types'
import styled, {keyframes, css} from 'styled-components'

import utils from 'Utils/utils'

import FireToken from './components/FireToken/FireToken'
import HotspotToken from './components/HotspotToken/HotspotToken'
import HazmatToken from './components/HazmatToken/HazmatToken'
import PoiToken from './components/PoiToken/PoiToken'

const StyledFloor = styled.td.attrs({
  lightgreen: '#90ee90',
  lightblue: '#add8e6',
  lightsalmon: '#ffa07a',
  lightgray: '#d3d3d3',
  invisiblecolor: 'rgba(255, 255, 255, 0)',
  set_bg_position: (props) => {
    const leftDif = 11.111 // 100% / 9 cols
    const topDif = 14.286 // 100% / 7 rows;
    const left = props.inherited.floor.col * leftDif
    const top = props.inherited.floor.row * topDif
    const newPos = `${left}% ${top}%`
    return newPos
  },
  selectable_animation: () => {
    const selectableColor = 'rgba(255, 255, 255, .4)'
    const invisiblecolor = 'rgba(255, 255, 255, 0)'
    return keyframes`
    0% {
      background-color: ${invisiblecolor};
    }
    50% {
      background-color: ${invisiblecolor};
    }
    75% {
      background-color: ${selectableColor};
    }
    100% {
      background-color: ${invisiblecolor};
    }`
  },
  get_animation_delay: () => {
    let delay = utils.getRandNumBetween(0, 2000)
    return `${delay}ms`
  }
})`
  position: relative;
  height: ${props => props.inherited.floorsDimensions.height + 'px'};
  background-image: ${props => `url("${props.inherited.boardBgImg}")`};
  background-size: ${props => props.inherited.bgSize};
  background-position: ${props => props.set_bg_position};
  width: 10%;
  margin: 0;
  padding: 0;
  text-align: center;
  vertical-align: top;
  border: 1px solid #CCC;

  ${props => props.inherited.floor.type === 'in' && css`
    background-color: ${props => props.lightgray};
  `}

  ${props => props.inherited.floor.type === 'out' && css`
    background-color: ${props => props.lightgreen};
  `}

  ${props => props.inherited.floor.isAmbulance && css`
    background-color: ${props => props.lightblue};
  `}

  ${props => props.inherited.floor.isTruck && css`
    background-color: ${props => props.lightsalmon};
  `}

  ${props => props.inherited.floor.selectable && css`
    cursor: pointer;

    ::after {
      display: block;
      content: '\A';
      color: ${props => props.invisiblecolor};
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      animation: ${props => props.selectable_animation} 4s linear infinite;
      animation-delay: ${props => props.get_animation_delay};
    }

    &:hover {
      ::after {
        display: block;
        animation: none;
        background-color: rgba(255,255,255,.5);
      }
    }
  `}
`

const Floor = ({floor, boardBgImg, bgSize, floorsDimensions, handleClick}) => {
  // console.log('Rendering Floor')
  return (
    <StyledFloor
      inherited={{floor, boardBgImg, bgSize, floorsDimensions, handleClick}}
      onClick={(e) => handleClick(floor)}>

      { Boolean(floor.hasHotspot) &&
          <HotspotToken />
      }

      { Boolean(floor.hasHazmat) &&
          <HazmatToken />
      }

      { Boolean(floor.fireStatus) &&
          <FireToken fireStatus={floor.fireStatus}/>
      }

      { Boolean(floor.poi) &&
          <PoiToken poi={floor.poi}/>
      }

    </StyledFloor>)
}

Floor.propTypes = {
  floor: PropTypes.object.isRequired,
  boardBgImg: PropTypes.string.isRequired,
  bgSize: PropTypes.string.isRequired,
  floorsDimensions: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired
}

export default Floor
