import React from 'react'
import PropTypes from 'prop-types'
import styled, {css} from 'styled-components'

import openDoorImg from './img/openDoor.png'
import closedDoorImg from './img/closedDoor.png'

const StyledBoardDoor = styled.div`
  height: 100%;
  padding: 0;
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  ${props => props.inherited.wallType === 'horizontal' && css`
    img {
      width: 45%;
    }
  `}

  ${props => props.inherited.wallType === 'vertical' && css`
    img {
      position: absolute;
      width: 350%;
    }
  `}
`

class BoardDoor extends React.Component {
  render() {
    return !this.props.door
      ? null
      : <StyledBoardDoor inherited={{ ...this.props, ...this.state }}>
        {
          this.props.door === 'open'
            ? <img src={`/${openDoorImg}`}/>
            : <img src={`/${closedDoorImg}`}/>
        }
      </StyledBoardDoor>
  }
}

BoardDoor.propTypes = {
  door: PropTypes.string,
  wallType: PropTypes.string.isRequired
}

export default BoardDoor
