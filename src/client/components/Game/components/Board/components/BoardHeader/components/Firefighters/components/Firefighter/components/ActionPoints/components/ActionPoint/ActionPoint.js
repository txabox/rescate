import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import actionPointImg from './img/actionPoint.png'
import extraMovementPointImg from './img/extraMovementPoint.png'
import extinguishPointImg from './img/extinguishPoint.png'
import freeCommandPointImg from './img/freeCommandPoint.png'

const images = {
  action: actionPointImg,
  moving: extraMovementPointImg,
  extinguishing: extinguishPointImg,
  freeCommand: freeCommandPointImg
}

const alts = {
  action: 'Action Points',
  moving: 'Extinguish Extra Points',
  extinguishing: 'Extra Moving Points',
  freeCommand: 'Free Command Points'
}

const StyledActionPoint = styled.div`
  img {
    display: inline-block;
    width: 100%;
  }
`

const ActionPoint = ({type, curPoints, totalPoints}) => {
  return <StyledActionPoint>
    <img src={`/${images[type]}`} alt={alts[type]} title={alts[type]} />
    {curPoints}/{totalPoints}
  </StyledActionPoint>
}

ActionPoint.propTypes = {
  type: PropTypes.string.isRequired,
  curPoints: PropTypes.number.isRequired,
  totalPoints: PropTypes.number.isRequired
}

export default ActionPoint
