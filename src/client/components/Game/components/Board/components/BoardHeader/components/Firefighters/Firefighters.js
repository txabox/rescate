import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as FirefightersActionCreators from 'Actions/FirefightersActionCreators'

import Firefighter from './components/Firefighter/Firefighter'

const StyledFirefighters = styled.div`
  .players-mngr-table {
    background-color: #5b5a58;
    margin: 0;
    width: 100%;
    height: 100%;

    td {
      color: #fff;
      overflow: hidden;
      vertical-align: middle;
      border: 1px solid #888;
      @media only screen and (max-width: 768px) {
        font-size: .7em;
      }
    }
  }
`

class Firefighters extends React.Component {
  printTable = () => {
    const {numOfFirefighters} = this.props
    const rowsNum = numOfFirefighters > 3 ? 2 : 1
    let colsNum = -1
    switch (numOfFirefighters) {
      case 1:
      case 2:
      case 4:
        colsNum = 2
        break
      default:
        colsNum = 3
    }
    let rows = []
    for (let i = 0; i < rowsNum; ++i) {
      let row = {
        id: i,
        cols: []
      }
      for (let j = 0; j < colsNum; ++j) {
        row.cols.push({
          id: j
        })
      }
      rows.push(row)
    }

    const tableData = {
      class: 'players-mngr-table',
      rows
    }

    const Table = ({data}) => {
      const TableRow = ({row}) => {
        const TableCol = ({row, col}) => {
          const self = this
          const curColFirefighterNum = colsNum * row.id + col.id + 1
          const height = self.props.numOfFirefighters > 3 ? self.props.headDimensions.height / 2 : self.props.headDimensions.height
          const elWidth = self.$el ? self.$el.offsetWidth : 0
          const width = self.props.numOfFirefighters === 2 || self.props.numOfFirefighters === 4 ? elWidth / 2 : elWidth / 3
          const showThisCol = (curColFirefighterNum) => {
            return numOfFirefighters >= curColFirefighterNum
          }

          return (
            (showThisCol(curColFirefighterNum)
              ? <td
                style={{
                  height: height + 'px',
                  width: width + 'px'
                }}>

                <Firefighter
                  height={height}
                  width={width}
                  numOfFirefighters={numOfFirefighters}
                  showExtendedInfo={this.props.actions.firefighters.showExtendedInfo}
                  firefighter={this.props.curFirefighters[curColFirefighterNum - 1]} />
              </td>
              : self.props.numOfFirefighters > 3 ? <td></td> : null
            )
          )
        }

        return <tr>
          {
            row.cols.map((col) => <TableCol key={row.id + '_' + col.id} row={row} col={col} />)
          }
        </tr>
      }
      return <table className={data.class}>
        <tbody>
          {
            data.rows.map((row) => <TableRow key={row.id} row={row} />)
          }
        </tbody>
      </table>
    }

    return <Table data={tableData} />
  }

  render() {
    // console.log('Rendering Firefighters')
    return <StyledFirefighters innerRef={div => (this.$el = div)}>
      {this.printTable()}
    </StyledFirefighters>
  }
}

Firefighters.propTypes = {
  headDimensions: PropTypes.object.isRequired,
  curFirefighters: PropTypes.array.isRequired,
  numOfFirefighters: PropTypes.number.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    curFirefighters: state.game.firefighters.array,
    numOfFirefighters: state.game.firefighters.array.length
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      firefighters: bindActionCreators(FirefightersActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Firefighters)
