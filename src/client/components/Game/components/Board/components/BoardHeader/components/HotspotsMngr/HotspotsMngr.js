import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { connect } from 'react-redux'

import hotspotImg from './img/hotspot.png'
import hotspotBkg from './img/hotspotBkg.png'

const StyledHotspotsMngr = styled.table`
  background-color: #5b5a58;
  margin: 0;
  height: 100%;

  > thead > tr > td {
    margin: 0;
    padding: 0;
    text-align: center;
    vertical-align: middle;
    border: none;
  }

  > tbody > tr > td {
    margin: 0;
    padding: 0;
    text-align: center;
    vertical-align: middle;
    background-image: url("/${hotspotBkg}");
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;
    border: none;
    img {
      width: 75%;
      display: block;
      margin: auto;
    }
  }
}
`

class HotspotsMngr extends React.Component {
  getLayout = () => {
    const self = this
    const TOTAL_HOTSPOT_SPACES = 6
    const cols = 2
    // let rows = Math.floor(TOTAL_HOTSPOT_SPACES / 2);

    let trArrays = []
    let tdArrays = []
    let curCounter = 1

    let tdHeight = ((self.props.headDimensions.height - (2 * self.props.hotspotsMngrPadding)) / 3) + 'px'

    for (var i = 1; i <= (TOTAL_HOTSPOT_SPACES / cols); i++) {
      let row = i
      tdArrays[row] = []

      for (var j = 1; j <= cols; j++) {
        let col = j
        tdArrays[row].push(
          <td
            style={{
              height: tdHeight,
              width: tdHeight
            }}
            key={row + '-' + col}>
            {((this.props.side === 'left' && this.props.hotspotsLeft >= curCounter) || (this.props.side === 'right' && this.props.hotspotsLeft >= TOTAL_HOTSPOT_SPACES + curCounter))
              ? <img
                src={`/${hotspotImg}`}
                style={{verticalAlign: 'middle'}}
                alt="Hotspot Token" />
              : null
            }
          </td>
        )

        curCounter++
      }

      trArrays.push(<tr key={row}>
        {tdArrays[row]}
      </tr>)
    }

    return trArrays
  }

  render() {
    if (this.props.headDimensions.height > 0) {
      // console.log('Rendering HotspotsMngr')
      return (
        <StyledHotspotsMngr>
          <tbody>

            {this.getLayout()}

          </tbody>
        </StyledHotspotsMngr>
      )
    }
    return null
  }
}

HotspotsMngr.propTypes = {
  side: PropTypes.string.isRequired,
  headDimensions: PropTypes.object.isRequired,
  hotspotsMngrPadding: PropTypes.number.isRequired,
  hotspotsLeft: PropTypes.number.isRequired
}

const mapStateToProps = (state) => {
  return {
    hotspotsLeft: state.game.settings.hotspotsLeft
  }
}

export default connect(mapStateToProps)(HotspotsMngr)
