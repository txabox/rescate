import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledName = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  background-color: rgba(0, 0, 0, .5);
  width: 100%;
  text-align: left;
  padding-left: 3%;
  overflow: hidden;
  font-size: ${props => props.numOfFirefighters === 2 ? 85 : 60}%;
  line-height: ${props => props.nameHeight}px;
  cursor: pointer;
`

const Name = ({height, firefighter, numOfFirefighters}) => {
  const nameHeight = (height / 4)
  return <StyledName numOfFirefighters={numOfFirefighters} nameHeight={nameHeight}>
    {firefighter.name}
  </StyledName>
}

Name.propTypes = {
  height: PropTypes.number.isRequired,
  firefighter: PropTypes.object.isRequired,
  numOfFirefighters: PropTypes.number.isRequired
}

export default Name
