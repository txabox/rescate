import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import ActionPoint from './components/ActionPoint/ActionPoint'

const StyledActionPoints = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  background-color: rgba(0, 0, 0, .5);
  height: 100%;
  width: 12%;
  max-width: 30px;
  padding: 1% 2%;
  text-align: center;
  font-size: .5em;
  cursor: pointer;

  @media (max-width: 400px) {
    font-size: .4em;
  }
`

const ActionPoints = ({firefighter}) => (
  <StyledActionPoints>
    <ActionPoint
      type="action"
      curPoints={firefighter.curAPs}
      totalPoints={firefighter.APs}
    />

    { Boolean(firefighter.extraAPs) && Boolean(firefighter.extraAPs.extinguishing) &&
      <ActionPoint
        type="extinguishing"
        curPoints={firefighter.extraAPs.curExtinguishing}
        totalPoints={firefighter.extraAPs.extinguishing}
      />
    }

    { Boolean(firefighter.extraAPs) && Boolean(firefighter.extraAPs.moving) &&
      <ActionPoint
        type="moving"
        curPoints={firefighter.extraAPs.curMoving}
        totalPoints={firefighter.extraAPs.moving}
      />
    }

    { Boolean(firefighter.extraAPs) && Boolean(firefighter.extraAPs.freeCommand) &&
      <ActionPoint
        type="freeCommand"
        curPoints={firefighter.extraAPs.curFreeCommand}
        totalPoints={firefighter.extraAPs.freeCommand}
      />
    }
  </StyledActionPoints>
)

ActionPoints.propTypes = {
  firefighter: PropTypes.object.isRequired
}

export default ActionPoints
