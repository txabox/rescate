import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import KilledSurvivorsMngr from './components/KilledSurvivorsMngr/KilledSurvivorsMngr'
import HotspotsMngr from './components/HotspotsMngr/HotspotsMngr'
import Firefighters from './components/Firefighters/Firefighters'
import SavedSurvivorsMngr from './components/SavedSurvivorsMngr/SavedSurvivorsMngr'

const StyledBoardHeader = styled.thead`
  .left-td,
  .right-td{
    width: 20%;
    background-color: #5b5a58;
  }

  .center-td{
    width: 60%;
  }

  .board-header-center-table{
    background-color: #5b5a58;
    width: 100%;


    .left-hotspots-manager-td,
    .right-hotspots-manager-td {
      width: 10%;
      height: ${props => props.headDimensions.height}px;
      padding: ${props => props.hotspotsMngrPadding}px;
    }

    .players-manager-td {
      width: 80%;
    }

  }

`

class BoardHeader extends React.Component {
  constructor(props) {
    super(props)

    this._hotspotsMngrPadding = 0
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.headDimensions.width !== nextProps.headDimensions.width) {
      return true
    }
    return false
  }

  render() {
    // console.log('Rendering BoardHeader')
    if (this.props.headDimensions.height > 0) {
      this._hotspotsMngrPadding = this.props.headDimensions.height / 20
    }
    return (
      <StyledBoardHeader
        headDimensions={this.props.headDimensions}
        hotspotsMngrPadding={this._hotspotsMngrPadding}
        innerRef={this.props.innerRef}>
        <tr>

          <td colSpan="2" className="left-td">
            <KilledSurvivorsMngr
              headDimensions={this.props.headDimensions}
            />
          </td>

          <td colSpan="6" className="center-td">

            <table className="board-header-center-table">
              <tbody>
                <tr>
                  <td className="left-hotspots-manager-td">
                    <HotspotsMngr
                      side="left"
                      headDimensions={this.props.headDimensions}
                      hotspotsMngrPadding={this._hotspotsMngrPadding} />
                  </td>
                  <td className="players-manager-td">
                    <Firefighters
                      headDimensions={this.props.headDimensions} />
                  </td>
                  <td className="right-hotspots-manager-td">
                    <HotspotsMngr
                      side="right"
                      headDimensions={this.props.headDimensions}
                      hotspotsMngrPadding={this._hotspotsMngrPadding} />
                  </td>
                </tr>
              </tbody>
            </table>

          </td>

          <td colSpan="2" className="right-td" >
            <SavedSurvivorsMngr
              headDimensions={this.props.headDimensions}
            />
          </td>

        </tr>
      </StyledBoardHeader>
    )
  }
}

BoardHeader.propTypes = {
  getParent: PropTypes.func.isRequired,
  innerRef: PropTypes.func.isRequired,
  headDimensions: PropTypes.object.isRequired
}

export default BoardHeader
