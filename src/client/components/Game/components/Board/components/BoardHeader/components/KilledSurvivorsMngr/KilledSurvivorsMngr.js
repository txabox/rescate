import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { connect } from 'react-redux'

import person1Img from './img/victims/person1.png'
import person2Img from './img/victims/person2.png'
import person3Img from './img/victims/person3.png'
import person4Img from './img/victims/person4.png'
import person5Img from './img/victims/person5.png'
import person6Img from './img/victims/person6.png'
import person7Img from './img/victims/person7.png'
import person8Img from './img/victims/person8.png'
import person9Img from './img/victims/person9.png'
import person10Img from './img/victims/person10.png'
import person11Img from './img/victims/person11.png'
import person12Img from './img/victims/person12.png'

import bgImg from './img/killedSurvivorsBackground.jpg'

const KilledSavedSurvivorMngr = styled.div`
  background-image: url("/${bgImg}");
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  background-color: #5b5a58;
  height: ${props => props.height}px;

  .survivors-table {
    background-color: rgba(0, 0, 0, 0);
    width: 85%;
    height: 100%;
    margin: 0 auto;

    td {
      background-size: contain;
      background-repeat: no-repeat;
      background-position: center;
      background-color: transparent;
    }
  }
`

class KilledSurvivorsMngr extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      imagesByName: {
        person1Img,
        person2Img,
        person3Img,
        person4Img,
        person5Img,
        person6Img,
        person7Img,
        person8Img,
        person9Img,
        person10Img,
        person11Img,
        person12Img
      }
    }
  }

  printTable = () => {
    const tableData = {
      class: 'survivors-table',
      rows: [
        {
          id: 1,
          cols: [
            {
              id: 1
            },
            {
              id: 2
            },
            {
              id: 3
            },
            {
              id: 4
            }
          ]
        },
        {
          id: 2,
          cols: [
            {
              id: 5
            },
            {
              id: 6
            },
            {
              id: 7
            },
            {
              id: 8
            }
          ]
        },
        {
          id: 3,
          cols: [
            {
              id: 9
            },
            {
              id: 10
            },
            {
              id: 11
            },
            {
              id: 12
            }
          ]
        }
      ]
    }

    const TableCol = ({col}) => {
      const isThisSurvivorKilled = (survivorId) => {
        return this.props.killedSurvivors.array.indexOf(survivorId) >= 0
      }
      return (
        <td key={col.id} style={ (isThisSurvivorKilled(col.id) ? {backgroundImage: `url("/${this.state.imagesByName['person' + col.id + 'Img']}")`} : {}) }></td>
      )
    }

    const TableRow = ({row}) => (
      <tr>
        {
          row.cols.map((col) => <TableCol key={col.id} col={col} />)
        }
      </tr>
    )

    const Table = ({data}) => (
      <table className={data.class}>
        <tbody>
          {
            data.rows.map((row) => <TableRow key={row.id} row={row} />)
          }
        </tbody>
      </table>
    )

    return <Table data={tableData} />
  }

  render() {
    // console.log('Rendering KilledSavedSurvivorMngr')
    return (
      <KilledSavedSurvivorMngr
        height={this.props.headDimensions.height}
        innerRef={div => (this.$el = div)}
      >
        {this.printTable()}
      </KilledSavedSurvivorMngr>
    )
  }
}

KilledSurvivorsMngr.propTypes = {
  headDimensions: PropTypes.object.isRequired,
  killedSurvivors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    killedSurvivors: state.game.killedSurvivors
  }
}

export default connect(mapStateToProps)(KilledSurvivorsMngr)
