import React from 'react'
import PropTypes from 'prop-types'
import styled, {keyframes, css} from 'styled-components'

import { connect } from 'react-redux'

import cafsImg from './img/cafs.jpg'
import driverImg from './img/driver.jpg'
import fireCaptainImg from './img/fireCaptain.jpg'
import generalistImg from './img/generalist.jpg'
import hazmatTechnicianImg from './img/hazmatTechnician.jpg'
import imagingTechnicianImg from './img/imagingTechnician.jpg'
import paramedicImg from './img/paramedic.jpg'
import rescueSpecialistImg from './img/rescueSpecialist.jpg'

const bgImageAspectRatio = 2.52 // 404px width / 160px height;

const StyledBgImage = styled.div.attrs({
  color_img_animation: () => {
    return keyframes`
    0%    { filter: grayscale(75%) brightness(1); }
    100%  { filter: grayscale(0%) brightness(1.5); }`
  },
  get_img_dimensions: (props) => {
    const curAspectRatio = props.width / props.height
    const newDim = {
      width: 'inherit',
      height: 'inherit'
    }
    if (curAspectRatio < bgImageAspectRatio) {
      newDim.height = props.height + 'px'
    } else {
      newDim.width = props.width + 'px'
    }
    return newDim
  }
})`
  position: absolute;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;
  width: 100%;
  height: ${props => props.height}px;

  img{
    width: ${props => props.get_img_dimensions.width};
    height: ${props => props.get_img_dimensions.height};
    ${props => props.isCurrent && css`
      filter: brightness(1.5);
      &:hover {
        filter: brightness(1.3);
      }
    `}

    ${props => !props.isCurrent && css`
      filter: grayscale(75%) brightness(1);
      &:hover {
        animation: ${props => props.color_img_animation} 100ms linear;
        animation-fill-mode: forwards;
      }
    `}
  }
`

class BgImage extends React.Component {
  constructor(props) {
    super(props)

    const IMAGES = {
      'cafs': cafsImg,
      'driver': driverImg,
      'fireCaptain': fireCaptainImg,
      'generalist': generalistImg,
      'hazmatTechnician': hazmatTechnicianImg,
      'imagingTechnician': imagingTechnicianImg,
      'paramedic': paramedicImg,
      'rescueSpecialist': rescueSpecialistImg
    }

    this.bgImage = '/' + IMAGES[props.firefighter._name]
  }

  render() {
    // console.log('Rendering BgImage')
    return <StyledBgImage
      isCurrent={this.props.activeFirefighter === this.props.firefighter._name}
      bgImage={this.bgImage}
      height={this.props.height}
      width={this.props.width}>
      <img src={this.bgImage} />
    </StyledBgImage>
  }
}

BgImage.propTypes = {
  firefighter: PropTypes.object.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  activeFirefighter: PropTypes.string.isRequired
}

const mapStateToProps = (state) => {
  return {
    activeFirefighter: state.game.firefighters.activeFirefighter
  }
}

export default connect(mapStateToProps)(BgImage)
