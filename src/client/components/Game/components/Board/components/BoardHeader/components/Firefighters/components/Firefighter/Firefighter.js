import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import ActionPoints from './components/ActionPoints/ActionPoints'
import Name from './components/Name/Name'
import BgImage from './components/BgImage/BgImage'

const StyledFirefighter = styled.div`
  position: relative;
  height: ${props => props.height}px;
`

const Firefighter = ({width, height, firefighter, numOfFirefighters, showExtendedInfo}) => {
  if (height === 0) return null

  const onClick = (e) => {
    showExtendedInfo(firefighter._name)
  }

  // console.log('Rendering Firefighter')
  return (
    <StyledFirefighter
      height={height}
      onClick={onClick}>

      <BgImage
        firefighter={firefighter}
        width={width}
        height={height} />

      <Name
        height={height}
        firefighter={firefighter}
        numOfFirefighters={numOfFirefighters} />

      <ActionPoints firefighter={firefighter} />

    </StyledFirefighter>

  )
}

Firefighter.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  firefighter: PropTypes.object.isRequired,
  numOfFirefighters: PropTypes.number.isRequired,
  showExtendedInfo: PropTypes.func.isRequired
}

export default Firefighter
