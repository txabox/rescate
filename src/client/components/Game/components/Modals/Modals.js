import React from 'react'

import FirefightersModal from './components/FirefightersModal/FirefightersModal'

const Modals = () => <div>
  <FirefightersModal />
</div>

export default Modals
