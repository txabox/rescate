import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Modal, Button, Image, Glyphicon } from 'react-bootstrap'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as FirefightersActionCreators from 'Actions/FirefightersActionCreators'

import cafs from './img/cafs.jpg'
import driver from './img/driver.jpg'
import fireCaptain from './img/fireCaptain.jpg'
import generalist from './img/generalist.jpg'
import hazmatTechnician from './img/hazmatTechnician.jpg'
import imagingTechnician from './img/imagingTechnician.jpg'
import paramedic from './img/paramedic.jpg'
import rescueSpecialist from './img/rescueSpecialist.jpg'

const StyledFirefightersModal = styled.div`
  height: 200px;

  .modal-dialog{
    margin-top: ${props => props.modalDialog.marginTop}px;
    max-width: ${props => props.modalDialog.maxWidth}px;
    max-height: ${props => props.modalDialog.maxHeight}px;
    overflow: scroll;
  }
  .modal-content{
    background-color: white;
  }

  .modal-body{
    position: relative;
    padding: ${props => props.modalBody.padding}px;
    max-height: ${props => props.modalBody.maxHeight}px;
    button{
      position: absolute;
      right: 2px;
      top: 5px;
      color: white;
    }
    button:hover{
      color: #CCC
    }
  }
`

class FirefightersModal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      images: {
        cafs,
        driver,
        fireCaptain,
        generalist,
        hazmatTechnician,
        imagingTechnician,
        paramedic,
        rescueSpecialist
      },
      _window: {
        width: 0,
        height: 0
      },
      modalDialog: {
        maxWidth: 0,
        maxHeight: 0,
        marginTop: 0,
        MIN_MARGIN: 10
      },
      modalBody: {
        padding: 5,
        maxWidth: 0,
        maxHeight: 0,
        img: {
          width: 409, // pict real w
          height: 585, // pict real h
          ratio: 0.699 // img.width / img.height
        }
      }
    }
  }

  componentDidMount() {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  updateWindowDimensions = () => {
    const windowW = window.innerWidth
    const windowH = window.innerHeight
    const {modalDialog, modalBody} = this.state
    const img = modalBody.img
    const IN_LANDSCAPE_MODE = windowW > windowH

    let bodyMaxHeight
    let bodyMaxWidth

    if (IN_LANDSCAPE_MODE) {
      bodyMaxHeight = windowH - 2 * modalBody.padding - 2 * modalDialog.MIN_MARGIN
      bodyMaxWidth = bodyMaxHeight * img.ratio
    } else {
      bodyMaxHeight = img.height
      bodyMaxWidth = windowW - 2 * modalBody.padding - 2 * modalDialog.MIN_MARGIN
    }

    const dialogMaxHeight = windowH - 2 * modalDialog.MIN_MARGIN

    const marginTop = (windowH - (bodyMaxHeight > img.height ? img.height : bodyMaxHeight)) / 2

    this.setState({
      _window: {
        ...this.state._window,
        width: windowW,
        height: windowH
      },
      modalDialog: {
        ...this.state.modalDialog,
        maxWidth: bodyMaxWidth,
        maxHeight: dialogMaxHeight,
        marginTop
      },
      modalBody: {
        ...this.state.modalBody,
        maxHeight: bodyMaxHeight,
        maxWidth: bodyMaxWidth
      }
    })
  }

  handleHide = () => {
    this.props.actions.firefighters.hideExtendedInfo()
  }

  render() {
    // console.log('Rendering FirefightersModal')
    const {shown} = this.props
    const {images, modalDialog, modalBody} = this.state
    if (!shown) {
      return null
    }

    return <StyledFirefightersModal
      className="modal-container"
      modalDialog={modalDialog}
      modalBody={modalBody} >
      <Modal
        show={Boolean(shown)}
        onHide={this.handleHide}
        container={this}
        bsSize="small"
      >
        <Modal.Body>
          <Image src={`/${images[shown]}`} responsive />
          <Button className="pull-right" bsStyle="link" bsSize="large" onClick={this.handleHide}>
            <Glyphicon glyph="remove" />
          </Button>
        </Modal.Body>
      </Modal>
    </StyledFirefightersModal>
  }
}

FirefightersModal.propTypes = {
  shown: PropTypes.string,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    shown: state.game.firefighters.shown
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      firefighters: bindActionCreators(FirefightersActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FirefightersModal)
