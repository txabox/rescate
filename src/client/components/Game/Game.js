import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import styled from 'styled-components'

import Header from 'Components/common/header/Header'
import LoadingSpinner from 'Components/common/loadingSpinner/LoadingSpinner'
import Board from './components/Board/Board'
import Modals from './components/Modals/Modals'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as GameActionCreators from 'Actions/GameActionCreators'

const StyledGame = styled.div`
  height: 100%
`

class Game extends React.Component {
  componentDidMount() {
    // create timers, listeners...
    if (!this.props.match.params.id) {
      this.props.history.replace('settings')
      return false
    }

    this.init()
  }

  componentWillUnmount() {
    this.props.actions.game.willUnmount()
  }

  init = () => {
    const gameId = this.props.match.params.id
    // 1. Retrieve game data
    axios.get(`/api/games/${gameId}`)
      .then((response) => {
        // 1.1 Init game in store
        const game = response.data
        this.props.actions.game.init(game)
      })
      .catch((error) => {
        // if there are any errors in promise chain
        // we can catch them in one place, yay!
        console.error('Error retrieving game ' + gameId, error)
      })
  }

  render() {
    // console.log('Rendering Game')
    return (
      <StyledGame>
        <Header />
        {
          !this.props.isInitialSetDone
            ? <LoadingSpinner />
            : <div className="container">
              <div className="row">
                <Board />
                <Modals />
              </div>
            </div>
        }
      </StyledGame>
    )
  }
}

Game.propTypes = {
  isInitialSetDone: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
  match: PropTypes.object,
  history: PropTypes.object
}

const mapStateToProps = (state) => {
  return {
    isInitialSetDone: state.game.settings.isInitialSetDone
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      game: bindActionCreators(GameActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Game)
