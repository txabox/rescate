import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ButtonGroup, Button } from 'react-bootstrap'

const StyledNumOfPlayers = styled.div``

const NumOfPlayers = ({numOfPlayers, minNumOfPlayers, maxNumOfPlayers, setNumOfPlayers}) => {
  const buttons = []
  for (let i = minNumOfPlayers; i <= maxNumOfPlayers; ++i) {
    buttons.push(
      <Button
        key={i}
        disabled
        bsStyle={(numOfPlayers === i ? 'success' : null)}
        onClick={(e) => setNumOfPlayers(Number(e.target.value))}
        active={numOfPlayers === i}
        value={i}>
        {i}
      </Button>
    )
  }
  return <StyledNumOfPlayers className="form-group row">
    <div className="col-sm-4 col-form-label text-sm-right text-xs-center">
      <label>Num of players:</label>
    </div>

    <div className="col-sm-8 text-sm-left text-xs-center">
      <ButtonGroup>
        {buttons}
      </ButtonGroup>

    </div>
  </StyledNumOfPlayers>
}

NumOfPlayers.propTypes = {
  numOfPlayers: PropTypes.number.isRequired,
  minNumOfPlayers: PropTypes.number.isRequired,
  maxNumOfPlayers: PropTypes.number.isRequired,
  setNumOfPlayers: PropTypes.func.isRequired
}

export default NumOfPlayers
