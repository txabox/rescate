import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import axios from 'axios'

import LoadingSpinner from 'Components/common/loadingSpinner/LoadingSpinner'
import AvailableBoard from './components/AvailableBoard/AvailableBoard'

const StyledAvailableBoards = styled.div`
  .available-boards-ul {
    list-style: none;
    padding-left: 0;

    li {
      display: inline-block;
      width: 50%;
      padding: 5px;
    }

    label {
      position: relative;

      input {
        visibility: hidden;
        position: absolute;

        + .container {
          border: 4px solid transparent;
        }

        &:checked + .container {
          border: 4px solid #74c374;
        }
      }

      .container {
        position: relative;
        width: 100%;
        background-color: #ebebeb;
        border-radius: 8px;
        padding: 10px 15px;
        cursor: pointer;

        &:hover {
          ::after {
            content: '\A';
            color: rgba(255, 255, 255, 0);
            width: 100%;
            height: 100% !important;
            background: rgba(255, 255, 255, .1);
            position: absolute;
            display: block;
            top: 0;
            left: 0;
          }
        }


        img {
          width: 100%;
        }

        p {
          margin: 8px auto 0;

          &.desc {
            margin-top: 0;
            font-weight: 400;
            font-size: 13px;
            color: #888;
          }
        }
      }
    }
  }
`

class AvailableBoards extends React.Component {
  componentDidMount() {
    axios.get('/api/boards')
      .then((response) => {
        if (response.status !== 200) {
          console.error('Error retrieving available Boards. response: ', response)
          alert('Error retrieving available Boards')
        }
        const availableBoards = response.data
        const selectedBoard = availableBoards[0]._id
        this.props.setAvailableBoards(availableBoards)
        this.props.setSelectedBoard(selectedBoard)
      })
      .catch((error) => {
        console.error('error: ', error)
      })
  }
  render() {
    return <StyledAvailableBoards className="form-group row">

      <div className="col-sm-4 col-form-label text-sm-right text-xs-center">
        <label>Available boards:</label>
      </div>

      <div className="col-sm-8 text-sm-left text-xs-center">
        {
        this.props.availableBoards.length === 0
        ? <LoadingSpinner />
        : <ul className='available-boards-ul'>
          {
            this.props.availableBoards.map((board) => {
              return <AvailableBoard
                key={board._id}
                board={board}
                selectedBoard={this.props.selectedBoard}
                setSelectedBoard={this.props.setSelectedBoard} />
            })
          }
        </ul>
        }

      </div>
    </StyledAvailableBoards>
  }
}

AvailableBoards.propTypes = {
  availableBoards: PropTypes.array.isRequired,
  selectedBoard: PropTypes.string.isRequired,
  setAvailableBoards: PropTypes.func.isRequired,
  setSelectedBoard: PropTypes.func.isRequired
}

export default AvailableBoards
