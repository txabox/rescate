import React from 'react'
import PropTypes from 'prop-types'
import { Image } from 'react-bootstrap'

const AvailableBoard = ({board, selectedBoard, setSelectedBoard}) => {
  return <li>
    <label>

      <input
        type="radio"
        name='availableBoardsGroup'
        value={board.settings.name}
        checked={selectedBoard === board._id}
        onChange={(e) => setSelectedBoard(e.target.value)} />

      <div className="container">
        <Image rounded src={`/assets/images/Game/Board/${board.settings.imageFilename}.jpg`} />
        <p>{board.settings.title}</p>
        <p className="desc">{board.settings.desc}</p>
      </div>

    </label>
  </li>
}

AvailableBoard.propTypes = {
  board: PropTypes.object.isRequired,
  selectedBoard: PropTypes.string.isRequired,
  setSelectedBoard: PropTypes.func.isRequired
}

export default AvailableBoard
