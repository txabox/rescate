import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { ButtonGroup, Button } from 'react-bootstrap'

const StyledDifficulty = styled.div``

const Difficulty = ({availableDifficulties, difficulty, setDifficulty}) => {
  const buttons = []
  for (let i = 0; i < availableDifficulties.length; ++i) {
    const curDifficulty = availableDifficulties[i]
    buttons.push(
      <Button
        key={curDifficulty}
        disabled={curDifficulty === 'basic'}
        bsStyle={(difficulty === curDifficulty ? 'success' : null)}
        onClick={(e) => setDifficulty(e.target.value)}
        active={difficulty === curDifficulty}
        value={curDifficulty}>
        {curDifficulty}
      </Button>
    )
  }
  return <StyledDifficulty className="form-group row">

    <div className="col-sm-4 col-form-label text-sm-right text-xs-center">
      <label>Difficulty:</label>
    </div>

    <div className="col-sm-8 text-sm-left text-xs-center">
      <ButtonGroup>
        {buttons}
      </ButtonGroup>
    </div>

  </StyledDifficulty>
}

Difficulty.propTypes = {
  availableDifficulties: PropTypes.array.isRequired,
  difficulty: PropTypes.string.isRequired,
  setDifficulty: PropTypes.func.isRequired
}

export default Difficulty
