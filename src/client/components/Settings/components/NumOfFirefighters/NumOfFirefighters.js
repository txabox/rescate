import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ButtonGroup, Button } from 'react-bootstrap'

const StyledNumOfFirefighters = styled.div``

const NumOfFirefighters = ({numOfFirefighters, minNumOfFirefighters, maxNumOfFirefighters, setNumOfFirefighters}) => {
  const buttons = []
  for (let i = minNumOfFirefighters; i <= maxNumOfFirefighters; ++i) {
    buttons.push(
      <Button
        key={i}
        bsStyle={(numOfFirefighters === i ? 'success' : null)}
        onClick={(e) => setNumOfFirefighters(Number(e.target.value))}
        active={numOfFirefighters === i}
        value={i}>
        {i}
      </Button>
    )
  }
  return <StyledNumOfFirefighters className="form-group row">

    <div className="col-sm-4 col-form-label text-sm-right text-xs-center">
      <label>Num of firefighters:</label>
    </div>

    <div className="col-sm-8 text-sm-left text-xs-center">
      <ButtonGroup>
        {buttons}
      </ButtonGroup>

    </div>
  </StyledNumOfFirefighters>
}

NumOfFirefighters.propTypes = {
  numOfFirefighters: PropTypes.number.isRequired,
  minNumOfFirefighters: PropTypes.number.isRequired,
  maxNumOfFirefighters: PropTypes.number.isRequired,
  setNumOfFirefighters: PropTypes.func.isRequired
}

export default NumOfFirefighters
