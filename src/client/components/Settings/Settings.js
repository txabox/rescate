import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-bootstrap'
import styled from 'styled-components'
import axios from 'axios'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as UsersActionCreators from 'Actions/UsersActionCreators'
import * as SettingsActionCreators from './SettingsActionCreators'

import Header from 'Components/common/header/Header'

import Difficulty from './components/Difficulty/Difficulty'
import NumOfPlayers from './components/NumOfPlayers/NumOfPlayers'
import NumOfFirefighters from './components/NumOfFirefighters/NumOfFirefighters'
import AvailableBoards from './components/AvailableBoards/AvailableBoards'

const StyledSettings = styled.div`
  height: 100%;
  .account-wall {
    margin-top: 20px;
    padding: 40px 30px 20px;
    background-color: #f7f7f7;
    box-shadow: 0 2px 2px rgba(0, 0, 0, .3);
  }

  h1 {
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
    text-align: center;
  }

  .create-game-btn-container {
    margin: 20px auto 25px;
    text-align: center;
  }
`

class Settings extends React.Component {
  componentWillUnmount() {
    this.props.actions.settings.willUnmount()
  }

  createGame = (e) => {
    const {selectedBoard, difficulty, numOfFirefighters, numOfPlayers} = this.props.settings
    const settings = {
      selectedBoard,
      difficulty,
      numOfFirefighters,
      numOfPlayers
    }
    axios.post('/api/games', settings)
      .then((response) => {
        if (response.status !== 200) {
          alert('Error creating new game. Look at the console to see more details')
          console.error('Error creating new game. response: ', response)
        }
        const newGameId = response.data._id

        this.props.history.push('/game/' + newGameId)
      })
      .catch((error) => {
        alert('Error creating new game. Look at the console to see more details')
        console.error('Error creating new game. error: ', error.response)
      })
  }

  render() {
    return (
      <StyledSettings>
        <Header />

        <div className="container">
          <div className="row">
            <div className="col-sm-8 col-sm-offset-2">

              <h1>Settings</h1>
              <div className="account-wall">

                <div className="row content">
                  <div className="col text-center">

                    <form>
                      <Difficulty
                        availableDifficulties={this.props.settings.availableDifficulties}
                        difficulty={this.props.settings.difficulty}
                        setDifficulty={this.props.actions.settings.setDifficulty} />

                      <NumOfPlayers
                        numOfPlayers={this.props.settings.numOfPlayers}
                        minNumOfPlayers={this.props.settings.minNumOfPlayers}
                        maxNumOfPlayers={this.props.settings.maxNumOfPlayers}
                        setNumOfPlayers={this.props.actions.settings.setNumOfPlayers} />

                      <NumOfFirefighters
                        numOfFirefighters={this.props.settings.numOfFirefighters}
                        minNumOfFirefighters={this.props.settings.minNumOfFirefighters}
                        maxNumOfFirefighters={this.props.settings.maxNumOfFirefighters}
                        setNumOfFirefighters={this.props.actions.settings.setNumOfFirefighters} />

                      <AvailableBoards
                        availableBoards={this.props.settings.availableBoards}
                        selectedBoard={this.props.settings.selectedBoard}
                        setAvailableBoards= {this.props.actions.settings.setAvailableBoards}
                        setSelectedBoard= {this.props.actions.settings.setSelectedBoard} />

                    </form>

                  </div>
                </div>

              </div>

              <div className="create-game-btn-container">
                <Button bsStyle="success" onClick={this.createGame}>
                      Create New Game
                </Button>
              </div>

            </div>
          </div>
        </div>

      </StyledSettings>

    )
  }
}

Settings.propTypes = {
  history: PropTypes.object,
  users: PropTypes.object.isRequired,
  settings: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    settings: state.settings
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      settings: bindActionCreators(SettingsActionCreators, dispatch),
      users: bindActionCreators(UsersActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
