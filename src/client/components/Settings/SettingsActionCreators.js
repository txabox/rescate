export const setDifficulty = difficulty => {
  return {
    type: 'SETTINGS_SET_DIFFICULTY',
    payload: {
      difficulty
    }
  }
}

export const setNumOfPlayers = numOfPlayers => {
  return {
    type: 'SETTINGS_SET_NUM_OF_PLAYERS',
    payload: {
      numOfPlayers
    }
  }
}

export const setNumOfFirefighters = numOfFirefighters => {
  return {
    type: 'SETTINGS_SET_NUM_OF_FIREFIGHTERS',
    payload: {
      numOfFirefighters
    }
  }
}

export const setAvailableBoards = availableBoards => {
  return {
    type: 'SETTINGS_SET_AVAILABLE_BOARDS',
    payload: {
      availableBoards
    }
  }
}

export const setSelectedBoard = selectedBoard => {
  return {
    type: 'SETTINGS_SET_SELECTED_BOARD',
    payload: {
      selectedBoard
    }
  }
}

export const willUnmount = () => {
  return {
    type: 'SETTINGS_WILL_UNMOUNT',
    payload: {
    }
  }
}
