import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'
import storeJS from 'store2'
import axios from 'axios'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as UsersActionCreators from 'Actions/UsersActionCreators'
import Header from 'Components/common/header/Header'
import LoadingSpinner from 'Components/common/loadingSpinner/LoadingSpinner'

let renderThisTime = true

class SessionRoute extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isAuthenticating: true,
      isLogged: false,
      forceRerender: false
    }
  }

  componentDidMount() {
    const token = storeJS.get('token')
    const params = {}
    const config = {
      headers: {'Authorization': 'Bearer ' + token}
    }
    if (!token) {
      this.setState({ isAuthenticating: false, isLogged: false })
    } else {
      axios.post('/users/authenticate', params, config)
        .then((response) => {
          // if (!response.data.success) {
          //   this.props.history.replace('/login')
          //   return false
          // }
          const data = response.data

          this.props.actions.users.onAuthenticated(data.username)
          this.setState({ isAuthenticating: false, isLogged: true })
        })
        .catch((error) => {
          console.error('Error authenticating: ', error)
          this.setState({ isAuthenticating: false, isLogged: false })
        })
    }
  }

  // NOTE: This  method ONLY ONLY ONLY affects the outer <Route /> render
  // that's the reason why we can return always false.
  // The router will be rendered the 1st time, because this method does not affect
  // first render to be triggered
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.isAuthenticating === true) {
      renderThisTime = false
      return false
    }

    if (
      this.props.path !== nextProps.path ||
      JSON.stringify(this.props.users) !== JSON.stringify(nextProps.users) || JSON.stringify(this.state) !== JSON.stringify(nextState)) {
      return true
    }
    return false
  }

  render() {
    const { isAuthenticating, isLogged } = this.state
    // console.log('Rendering SessionRoute')
    return (
      <Route {...this.props} render={rest => {
        if (!renderThisTime) {
          renderThisTime = true
          return null
        }
        // console.log('Rendering Route', this.props, this.state)

        const success = <this.props.routeComponent {...rest} isLogged={this.state.isLogged} />

        const error = (this.props.needsLogin
          ? <Redirect to={{
            pathname: '/login'
          }}/>
          : success
        )

        if (isAuthenticating) {
          return <div className="full-height">
            <Header
              isAuthenticating={isAuthenticating} />
            <LoadingSpinner />
          </div>
        }

        return isLogged ? success : error
      }}/>
    )
  }
}

SessionRoute.propTypes = {
  users: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  path: PropTypes.string.isRequired,
  needsLogin: PropTypes.bool
}

const mapStateToProps = (state) => {
  return {
    users: state.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      users: bindActionCreators(UsersActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SessionRoute)
