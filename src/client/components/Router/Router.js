import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'

import SessionRoute from './components/SessionRoute/SessionRoute'

import Index from 'Components/Index/Index'
import Settings from 'Components/Settings/Settings'
import Game from 'Components/Game/Game'
import Login from 'Components/users/Login/Login'
import Signup from 'Components/users/Signup/Signup'

const Router = () => (
  <BrowserRouter>
    <Switch>
      <SessionRoute exact path="/" name="index" routeComponent={Index} />
      <SessionRoute path="/settings" name="settings" routeComponent={Settings} needsLogin />
      <SessionRoute path="/login" name="login" routeComponent={Login} />
      <SessionRoute path="/signup" name="signup" routeComponent={Signup} />
      <SessionRoute path="/game/:id?" name="game" routeComponent={Game} needsLogin />
    </Switch>
  </BrowserRouter>
)

export default Router
