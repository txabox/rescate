import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import storeJS from 'store2'

import styled from 'styled-components'

import { Link } from 'react-router-dom'
import { Navbar, Nav, NavItem, Glyphicon, NavDropdown, MenuItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as UsersActionCreators from 'Actions/UsersActionCreators'

const StyledHeader = styled.div`
  .navbar-default {
    margin-bottom: 0;
  }
`

class Header extends React.Component {
  logout = () => {
    const params = {}
    const config = {
      headers: {'Authorization': 'Bearer ' + storeJS.get('token')}
    }
    axios.post('/users/logout', params, config)
      .then((result) => {
        this.props.actions.logout()

        storeJS.remove('token')
        window.location.reload()
      })
      .catch(error => {
        console.error(error.response)
        alert('Error: (' + error.response.status + ') ' + error.response.statusText)
      })
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.props.isAuthenticating !== nextProps.isAuthenticating ||
      JSON.stringify(this.props.users) !== JSON.stringify(nextProps.users)) {
      return true
    }
    return false
  }

  render() {
    // console.log('Header: ', this.props.isAuthenticating, this.props.users.isLogged)
    return (
      <StyledHeader>

        <Navbar collapseOnSelect className="navbar-static-top">
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/"><Glyphicon glyph="home" /></Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            {
              this.props.isAuthenticating
              ? <Nav pullRight>
                <NavItem eventKey={1}>Authenthicating...</NavItem>
              </Nav>
            : this.props.users.isLogged
              ? <Nav pullRight>
                <NavDropdown eventKey={1} title={this.props.users.username} id="basic-nav-dropdown">
                  <MenuItem onClick={this.logout} eventKey={1.1}>Logout</MenuItem>
                </NavDropdown>
              </Nav>
              : <Nav pullRight>
                <LinkContainer to="/login">
                  <NavItem eventKey={1}>
                    Login
                  </NavItem>
                </LinkContainer>

                <LinkContainer to="/signup">
                  <NavItem eventKey={2}>
                    Sign up
                  </NavItem>
                </LinkContainer>
              </Nav>

            }

          </Navbar.Collapse>
        </Navbar>

      </StyledHeader>
    )
  }
}

Header.propTypes = {
  users: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  isAuthenticating: PropTypes.bool
}

const mapStateToProps = (state) => {
  return {
    users: state.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(UsersActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
