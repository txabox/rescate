const defaultValues = {
  isLogged: false,
  username: ''
}

const usersReducer = (state = defaultValues, action) => {
  let newState = {
    ...state
  }

  if (action.type === 'USERS_LOGIN') {
    newState.isLogged = true
    newState.username = action.payload.username
  } else if (action.type === 'USERS_ON_AUTHENTICATED') {
    newState.isLogged = true
    newState.username = action.payload.username
  } else if (action.type === 'USERS_LOGOUT') {
    newState.isLogged = false
    newState.username = ''
  }

  return newState
}

export default usersReducer
