import { combineReducers } from 'redux'
import settingsReducer from './gameSettingsReducer'
import boardReducer from './reducers/board/boardReducer'
import firefightersReducer from './reducers/firefighters/firefightersReducer'
import poisReducer from './reducers/pois/poisReducer'
import killedSurvivorsReducer from './reducers/killedSurvivors/killedSurvivorsReducer'
import savedSurvivorsReducer from './reducers/savedSurvivors/savedSurvivorsReducer'

const gameReducer = combineReducers({
  settings: settingsReducer,
  board: boardReducer,
  firefighters: firefightersReducer,
  pois: poisReducer,
  killedSurvivors: killedSurvivorsReducer,
  savedSurvivors: savedSurvivorsReducer
})

export default gameReducer
