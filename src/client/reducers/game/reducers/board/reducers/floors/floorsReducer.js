const defaultValues = {
  array: [],
  associative: {},
  dimensions: {
    width: 0,
    height: 0
  },
  bgSize: '0px 0px'
}

const floorsReducer = (state = defaultValues, action) => {
  function createAssociativeFromArray(array) {
    let newAssociative = {}
    const floors = array

    // fullfill newAssociative
    for (let i = 0; i < floors.length; ++i) {
      for (let j = 0; j < floors[i].length; ++j) {
        let floor = floors[i][j]

        newAssociative[floor.id] = floor
      }
    }
    return newAssociative
  }

  function updateElementInThisObject(newState, floorId, key, value) {
    function createNewArrayFromArray(origin) {
      return JSON.parse(JSON.stringify(origin))
    }

    // 1. transform newState array to be a brand new mutate-risk-free array
    newState.array = createNewArrayFromArray(newState.array)

    // 2. change the floor you want to change in the new mutated-risk-free array:
    for (let i = 0; i < newState.array.length; ++i) {
      let curRow = newState.array[i]
      for (let j = 0; j < curRow.length; ++j) {
        let curFloor = curRow[j]
        if (curFloor.id === floorId) {
          curFloor[key] = value
          i = newState.array.length // break the outer [i] loop
          break
        }
      }
    }

    // 3. recreate associative:
    newState.associative = createAssociativeFromArray(newState.array)
  }

  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    const floors = action.payload.game.board.floors
    newState.array = floors
    newState.associative = createAssociativeFromArray(newState.array)
  } else if (action.type === 'BOARD_SET_BODY_DIMENSIONS') {
    const setDimensions = () => {
      const floorDimensions = {}
      floorDimensions.width = action.payload.bodyDimensions.width / action.payload.COLS
      floorDimensions.height = floorDimensions.width // its a square
      newState.dimensions = floorDimensions
    }
    const setBgSize = () => {
      const textureRealWidth = action.payload.TEXTURE_WIDTH
      const idealFloorWidth = textureRealWidth / action.payload.COLS
      const curWidthProportion = (100 * newState.dimensions.width) / idealFloorWidth

      const newBgWidth = (textureRealWidth * curWidthProportion) / 100

      const textureRealHeight = action.payload.TEXTURE_HEIGHT
      const idealFloorHeight = textureRealHeight / action.payload.ROWS
      const curHeightProportion = (100 * newState.dimensions.height) / idealFloorHeight

      const newBgHeight = (action.payload.TEXTURE_HEIGHT * curHeightProportion) / 100
      newState.bgSize = newBgWidth + 'px ' + newBgHeight + 'px'
    }

    setDimensions()
    setBgSize()
  } else if (action.type === 'BOARDFLOORS_SET_FIRE_STATUS') {
    const floorId = action.payload.boardFloorId
    const fireStatus = action.payload.fireStatus
    updateElementInThisObject(newState, floorId, 'fireStatus', fireStatus)

    // NOTE: utils.getFloorById(floorId) gives you the ***PREVIOUS*** state floor
    // NOTE: to get the ***NEW*** state floor, do this: newState.associative[floorId];
  } else if (action.type === 'BOARDFLOORS_SET_HAS_HOTSPOT') {
    const floorId = action.payload.boardFloorId
    const hasHotspot = action.payload.hasHotspot
    updateElementInThisObject(newState, floorId, 'hasHotspot', hasHotspot)
  } else if (action.type === 'BOARDFLOORS_SET_HAS_HAZMAT') {
    const floorId = action.payload.boardFloorId
    const hasHazmat = action.payload.hasHazmat
    updateElementInThisObject(newState, floorId, 'hasHazmat', hasHazmat)
  } else if (action.type === 'BOARDFLOORS_ADD_NEW_POI') {
    const floorId = action.payload.boardFloorId
    const poi = {
      isVisible: false,
      id: action.payload.poi
    }
    updateElementInThisObject(newState, floorId, 'poi', poi)
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default floorsReducer
