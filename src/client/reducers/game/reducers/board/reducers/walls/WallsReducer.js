const defaultValues = {
  array: [],
  associative: {},
  dimensions: {
    horizontal: {
      width: 0,
      height: 0,
      halfW: 0,
      halfH: 0,
      floorW: 0,
      floorH: 0
    },
    vertical: {
      width: 0,
      height: 0,
      halfW: 0,
      halfH: 0,
      floorW: 0,
      floorH: 0
    }
  }
}

const wallsReducer = (state = defaultValues, action) => {
  function createAssociativeFromArray(array) {
    let newAssociative = {}
    const walls = array

    // fullfill newAssociative
    for (let i = 0; i < walls.length; ++i) {
      let wall = walls[i]
      newAssociative[wall.id] = wall
    }

    return newAssociative
  }

  function updateElementInThisObject(newState, wallId, key, value) {
    function createNewArrayFromArray(origin) {
      return JSON.parse(JSON.stringify(origin))
    }

    // 1. transform newState array to be a brand new mutate-risk-free array
    newState.array = createNewArrayFromArray(newState.array)

    // 2. change the floor you want to change in the new mutated-risk-free array:
    for (let i = 0; i < newState.array.length; ++i) {
      let curWall = newState.array[i]

      if (curWall.id === wallId) {
        curWall[key] = value
        break
      }
    }

    // 3. recreate associative:
    newState.associative = createAssociativeFromArray(newState.array)
  }

  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    const walls = action.payload.game.board.walls
    // create connections:
    for (let i = 0; i < walls.length; ++i) {
      const wall = walls[i]
      wall.connections = wall.id.split('-')
    }

    newState.array = walls
    newState.associative = createAssociativeFromArray(newState.array)
  } else if (action.type === 'BOARD_SET_BODY_DIMENSIONS') {
    const setDimensions = () => {
      const floorW = action.payload.bodyDimensions.width / action.payload.COLS
      const floorH = floorW
      const smallDim = floorH / 8

      const horizontal = {
        width: floorW,
        height: smallDim,
        halfW: floorW / 2,
        halfH: smallDim / 2,
        floorW,
        floorH
      }
      const vertical = {
        width: smallDim,
        height: floorH,
        halfW: smallDim / 2,
        halfH: floorH / 2,
        floorW,
        floorH
      }
      newState.dimensions = {horizontal, vertical}
    }

    setDimensions()
  } else if (action.type === 'BOARDWALLS_SET_DAMAGE') {
    const wallId = action.payload.wallId
    const damage = action.payload.damage
    updateElementInThisObject(newState, wallId, 'damage', damage)
  } else if (action.type === 'BOARDWALLS_SET_DOOR') {
    const wallId = action.payload.wallId
    const door = action.payload.door
    updateElementInThisObject(newState, wallId, 'door', door)
  } else if (action.type === 'BOARDWALLS_SET_SELECTABLE') {
    const wallId = action.payload.wallId
    const selectable = action.payload.selectable
    updateElementInThisObject(newState, wallId, 'selectable', selectable)
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default wallsReducer
