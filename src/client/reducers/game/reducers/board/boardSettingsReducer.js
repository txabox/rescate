const defaultValues = {
  HEAD_PROPORTION: 9.2777777, // 1169 px width / 126 px height
  COLS: 10,
  ROWS: 8,
  TEXTURE_WIDTH: 1270, // in pixels
  TEXTURE_HEIGHT: 1160, // in pixels
  name: '',
  title: '',
  desc: '',
  imageFilename: '',
  bgImg: '',
  headDimensions: {
    bottom: 0,
    height: 0,
    left: 0,
    right: 0,
    top: 0,
    width: 0
  },
  bodyDimensions: {
    bottom: 0,
    height: 0,
    left: 0,
    right: 0,
    top: 0,
    width: 0
  }
}

const settingsReducer = (state = defaultValues, action) => {
  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    newState = action.payload.game.board.settings
  } else if (action.type === 'BOARD_SET_BG_IMAGE') {
    newState.bgImg = action.payload.bgImg
  } else if (action.type === 'BOARD_SET_HEAD_DIMENSIONS') {
    newState.headDimensions = action.payload.headDimensions
  } else if (action.type === 'BOARD_SET_BODY_DIMENSIONS') {
    newState.bodyDimensions = action.payload.bodyDimensions
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default settingsReducer
