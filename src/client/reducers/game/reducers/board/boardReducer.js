import { combineReducers } from 'redux'
import settingsReducer from './boardSettingsReducer'
import wallsReducer from './reducers/walls/WallsReducer'
import floorsReducer from './reducers/floors/floorsReducer'

const boardReducer = combineReducers({
  settings: settingsReducer,
  walls: wallsReducer,
  floors: floorsReducer
})

export default boardReducer
