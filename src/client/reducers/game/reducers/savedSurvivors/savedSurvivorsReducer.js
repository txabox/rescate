const defaultValues = {
  // A pool of all points of interest left in the game
  array: []
}

const savedSurvivorsReducer = (state = defaultValues, action) => {
  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    newState = action.payload.game.savedSurvivors
  } else if (action.type === 'SAVEDSURVIVORS_ADD_ONE') {
    newState.array = newState.array.slice()
    newState.array.push(action.payload.savedSurvivor)
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default savedSurvivorsReducer
