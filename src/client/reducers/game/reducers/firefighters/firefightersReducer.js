const defaultValues = {
  // A pool of all firefigthers in game
  array: [],
  // the _name property of the current firefighter
  activeFirefighter: '',
  // the _name of the firefighter that is shown in the modal
  shown: ''
}

const firefightersReducer = (state = defaultValues, action) => {
  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    newState = action.payload.game.firefighters
  } else if (action.type === 'FIREFIGHTERS_SET_CUR') {
    newState.activeFirefighter = action.payload.activeFirefighter._name
  } else if (action.type === 'FIREFIGHTERS_SHOW_EXTENDED_INFO') {
    newState.shown = action.payload.firefighterName
  } else if (action.type === 'FIREFIGHTERS_HIDE_EXTENDED_INFO') {
    newState.shown = null
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default firefightersReducer
