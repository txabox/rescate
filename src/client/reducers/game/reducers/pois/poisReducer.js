const defaultValues = {
  // A pool of all points of interest left in the game
  array: []
}

const poisReducer = (state = defaultValues, action) => {
  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    newState = action.payload.game.pois
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default poisReducer
