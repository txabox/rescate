const defaultValues = {
  isInitialSetDone: false,
  difficulty: 0,
  numOfPlayers: 1,
  hotspotsLeft: 0
}

const settingsReducer = (state = defaultValues, action) => {
  let newState = {
    ...state
  }

  if (action.type === 'GAME_INIT') {
    newState = action.payload.game.settings
    newState.isInitialSetDone = true
  } else if (action.type === 'GAME_ADD_SAVED_SURVIVOR') {
    newState.savedSurvivors = newState.savedSurvivors.slice()
    newState.savedSurvivors.push(action.payload.savedSurvivor)
  } else if (action.type === 'GAME_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default settingsReducer
