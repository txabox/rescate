const defaultValues = {
  availableBoards: [],
  availableDifficulties: ['basic', 'recruit', 'veteran', 'heroic'],
  difficulty: 'recruit',
  numOfPlayers: 1,
  minNumOfPlayers: 1,
  maxNumOfPlayers: 6,
  numOfFirefighters: 6,
  minNumOfFirefighters: 2,
  maxNumOfFirefighters: 6,
  selectedBoard: ''
}

const settingsReducer = (state = defaultValues, action) => {
  let newState = {...state}

  if (action.type === 'SETTINGS_SET_DIFFICULTY') {
    newState.difficulty = action.payload.difficulty
  } else if (action.type === 'SETTINGS_SET_NUM_OF_PLAYERS') {
    newState.numOfPlayers = action.payload.numOfPlayers
  } else if (action.type === 'SETTINGS_SET_NUM_OF_FIREFIGHTERS') {
    newState.numOfFirefighters = action.payload.numOfFirefighters
  } else if (action.type === 'SETTINGS_SET_SELECTED_BOARD') {
    newState.selectedBoard = action.payload.selectedBoard
  } else if (action.type === 'SETTINGS_SET_AVAILABLE_BOARDS') {
    newState.availableBoards = action.payload.availableBoards
  } else if (action.type === 'SETTINGS_WILL_UNMOUNT') {
    newState = defaultValues
  }

  return newState
}

export default settingsReducer
