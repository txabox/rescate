export const addKilledSurivor = killedSurvivor => {
  return {
    type: 'KILLEDSURVIVORS_ADD_ONE',
    payload: {
      killedSurvivor
    }
  }
}
