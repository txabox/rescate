export const login = (username) => {
  return {
    type: 'USERS_LOGIN',
    payload: {
      username
    }
  }
}

export const onAuthenticated = (username) => {
  return {
    type: 'USERS_ON_AUTHENTICATED',
    payload: {
      username
    }
  }
}

export const logout = () => {
  return {
    type: 'USERS_LOGOUT',
    payload: {
    }
  }
}
