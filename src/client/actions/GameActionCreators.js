export const init = game => {
  return {
    type: 'GAME_INIT',
    payload: {
      game
    }
  }
}

export const willUnmount = () => {
  return {
    type: 'GAME_WILL_UNMOUNT',
    payload: {
    }
  }
}
