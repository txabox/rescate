export const setFireStatus = (boardFloorId, fireStatus) => {
  return {
    type: 'BOARDFLOORS_SET_FIRE_STATUS',
    payload: {
      boardFloorId,
      fireStatus
    }
  }
}

export const setHasHotspot = (boardFloorId, hasHotspot) => {
  return {
    type: 'BOARDFLOORS_SET_HAS_HOTSPOT',
    payload: {
      boardFloorId,
      hasHotspot
    }
  }
}

export const setHasHazmat = (boardFloorId, hasHazmat) => {
  return {
    type: 'BOARDFLOORS_SET_HAS_HAZMAT',
    payload: {
      boardFloorId,
      hasHazmat
    }
  }
}

export const addNewPOI = (boardFloorId, poi) => {
  return {
    type: 'BOARDFLOORS_ADD_NEW_POI',
    payload: {
      boardFloorId,
      poi
    }
  }
}
