export const setBgImg = bgImg => {
  return {
    type: 'BOARD_SET_BG_IMAGE',
    payload: {
      bgImg
    }
  }
}

export const setHeadDimensions = headDimensions => {
  return {
    type: 'BOARD_SET_HEAD_DIMENSIONS',
    payload: {
      headDimensions
    }
  }
}

export const setBodyDimensions = (bodyDimensions, TEXTURE_WIDTH, TEXTURE_HEIGHT, COLS, ROWS) => {
  return {
    type: 'BOARD_SET_BODY_DIMENSIONS',
    payload: {
      bodyDimensions,
      TEXTURE_WIDTH,
      TEXTURE_HEIGHT,
      COLS,
      ROWS
    }
  }
}
