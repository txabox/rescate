export const setCurFirefighter = activeFirefighter => {
  return {
    type: 'FIREFIGHTERS_SET_CUR',
    payload: {
      activeFirefighter
    }
  }
}

export const showExtendedInfo = firefighterName => {
  return {
    type: 'FIREFIGHTERS_SHOW_EXTENDED_INFO',
    payload: {
      firefighterName
    }
  }
}

export const hideExtendedInfo = () => {
  return {
    type: 'FIREFIGHTERS_HIDE_EXTENDED_INFO',
    payload: {
    }
  }
}
