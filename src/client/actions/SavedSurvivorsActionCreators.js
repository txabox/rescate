export const addSavedSurivor = savedSurvivor => {
  return {
    type: 'SAVEDSURVIVORS_ADD_ONE',
    payload: {
      savedSurvivor
    }
  }
}
