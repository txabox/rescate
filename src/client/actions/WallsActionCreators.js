export const setDamage = (wallId, damage) => {
  return {
    type: 'BOARDWALLS_SET_DAMAGE',
    payload: {
      wallId,
      damage
    }
  }
}

export const setDoor = (wallId, door) => {
  return {
    type: 'BOARDWALLS_SET_DOOR',
    payload: {
      wallId,
      door
    }
  }
}

export const setIsSelectable = (wallId, selectable) => {
  return {
    type: 'BOARDWALLS_SET_SELECTABLE',
    payload: {
      wallId,
      selectable
    }
  }
}
