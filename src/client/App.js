import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import store from './store'

// This styles should be loaded before the rest of the components
// because app styles have to be the 1st one (globals)
import './App.scss'

import Router from 'Components/Router/Router'

if (process.env.NODE_ENV !== 'production') {
  console.warn('Looks like we are in development mode!')
}

ReactDOM.render(
  (
    <Provider store={store}>
      <div className="full-height">
        <Router />
      </div>
    </Provider>
  ),
  document.getElementById('flashpointApp')
)
