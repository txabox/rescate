import axios from 'axios'
import storeJS from 'store2'

class Utils {
  static isEmptyObject(obj) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) { return false }
    }

    return JSON.stringify(obj) === JSON.stringify({})
  }

  isAuthenticated = () => {
    const token = storeJS.get('token')
    const params = {}
    const config = {
      headers: {'Authorization': 'Bearer ' + token}
    }
    return axios.post('/users/authenticate', params, config)
  }

  isInt(n) {
    return Number(n) === n && n % 1 === 0
  }

  getRandNumBetween = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }

  /**
   * Shuffles array in place. ES6 version
   * @param {Array} a items An array containing the items.
   */
  shuffleArray = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]]
    }
    return a
  }

  getWallById(id, walls) {
    if (walls.associative) { return walls.associative[ id ] }

    for (let i = 0; i < walls.length; ++i) {
      const wall = walls[i]
      if (wall.id === id) {
        return wall
        // break
      }
    }
  }

  getFloorById(id, floors) {
    if (floors.associative) { return floors.associative[ id ] }

    for (let i = 0; i < floors.length; ++i) {
      const curRowFloors = floors[i]
      for (var j = 0; j < curRowFloors.length; j++) {
        const floor = curRowFloors[j]
        if (floor.id === id) {
          return floor
          // break
        }
      }
    }
  }

  getFloorByDiceResult(diceResult, floors) {
    const id = diceResult.rows + '_' + diceResult.cols
    return this.getFloorById(id, floors)
  }

  rollRowsDie() {
    return Math.floor(Math.random() * 6) + 1
  }

  reRollRowsDie(curVal) {
    return {
      rows: this.rollRowsDie(),
      cols: curVal.cols
    }
  }

  swapRowsDie(curVal) {
    // if integer (not dice roll object {rows:n, cols:n})
    if (this.isInt(curVal)) {
      return 7 - curVal
      // curVal is equal to a dice roll with this format {rows:n, cols:n}
    } else {
      return {
        rows: this.swapRowsDie(curVal.rows),
        cols: curVal.cols
      }
    }
  }

  rollColsDie() {
    return Math.floor(Math.random() * 8) + 1
  }

  reRollColsDie(curVal) {
    return {
      rows: curVal.rows,
      cols: curVal.rollColsDie()
    }
  }

  swapColsDie(curVal) {
    // if integer (not dice roll object {rows:n, cols:n})
    if (this.isInt(curVal)) {
      switch (curVal) {
        case 1:
          return 6
        case 2:
          return 5
        case 3:
          return 8
        case 4:
          return 7
        case 5:
          return 2
        case 6:
          return 1
        case 7:
          return 4
        case 8:
          return 3
        default:
          console.error('Imposible result')
          break
      }

      // curVal is equal to a dice roll with this format {rows:n, cols:n}
    } else {
      return {
        rows: curVal.rows,
        cols: this.swapColsDie(curVal.cols)
      }
    }
  }

  rollDices() {
    return {
      rows: this.rollRowsDie(),
      cols: this.rollColsDie()
    }
  }

  swapDices(dices) {
    const {rows, cols} = dices
    return {
      rows: this.swapRowsDie(rows),
      cols: this.swapColsDie(cols)
    }
  }

  rollUntilValidFloor = (prohibitions, floors) => {
    let isProperFloor = true

    const diceResult = this.rollDices()
    const floor = this.getFloorById(this.getFloorIdFromDiceResult(diceResult), floors)

    // 1. check prohibitions
    for (let i = 0; i < prohibitions.length; ++i) {
      const prohibition = prohibitions[i]
      let floorPropertyToCheck = floor[prohibition.key]
      if (typeof (prohibition.value) === 'boolean') {
        floorPropertyToCheck = Boolean(floorPropertyToCheck)
      }

      if (floorPropertyToCheck === prohibition.value) {
        isProperFloor = false
        break
      }
    }

    // 3. If the Target space is already on Fire, roll just the red die again.
    if (!isProperFloor) {
      return this.rollUntilValidFloor(prohibitions, floors)
    } else {
      return diceResult
    }
  };

  getRandomValidFloor = (prohibitions, floors) => {
    const diceResult = this.rollUntilValidFloor(prohibitions, floors)
    return this.getFloorByDiceResult(diceResult, floors)
  }

  getDiceResultFromFloorId = (floorId) => {
    const tempArray = floorId.split('_')
    return {
      rows: tempArray[0],
      cols: tempArray[1]
    }
  }

  getFloorIdFromDiceResult = (diceResult) => {
    return diceResult.rows + '_' + diceResult.cols
  }

  // helper functions to deal with immutating arrays as seen here: https://github.com/reactjs/redux/blob/master/docs/recipes/reducers/ImmutableUpdatePatterns.md
  insertItemInArray(array, action) {
    let newArray = array.slice()
    newArray.splice(action.index, 0, action.item)
    return newArray
  }

  removeItemFromArray(array, index) {
    let newArray = array.slice()
    newArray.splice(index, 1)
    return newArray
  }

  updateObjectInArray(array, action) {
    const newArray = array.map((item, index) => {
      if (index !== action.index) {
        // This isn't the item we care about - keep it as-is
        return item
      }

      // Otherwise, this is the one we want - return an updated value
      return {
        ...item,
        ...action.item
      }
    })

    return newArray
  }

  cloneArrayInAnImmutableWay = (array) => {
    return JSON.parse(JSON.stringify(array))
  }

  // inmutably update element in array
  immutablyUpdateElementInArray = (array, elId, key, value, curArray) => {
    let immutableArray = null
    if (!curArray) {
      // 1. transform array array to be a brand new mutate-risk-free array
      immutableArray = this.cloneArrayInAnImmutableWay(array)
      curArray = immutableArray
    }

    // 2. change the floor you want to change in the new mutated-risk-free array:
    for (let i = 0; i < curArray.length; ++i) {
      const curElOrArray = curArray[i]
      if (!(curElOrArray instanceof Array)) {
        if (curElOrArray.id === elId) {
          curElOrArray[key] = value
          return curArray
          // break
        }
      } else {
        const result = this.immutablyUpdateElementInArray(immutableArray, elId, key, value, curElOrArray)
        if (result) {
          return curArray
        }
      }
    }

    return null
  }

  updateElementInArray = (array, elId, key, value) => {
    for (let i = 0; i < array.length; ++i) {
      const curElOrArray = array[i]
      if (!(curElOrArray instanceof Array)) {
        if (curElOrArray.id === elId) {
          curElOrArray[key] = value
          return array
          // break
        }
      } else {
        const result = this.updateElementInArray(curElOrArray, elId, key, value)
        if (result) {
          return array
        }
      }
    }

    return null
  }
}

const utils = new Utils()

export default utils
